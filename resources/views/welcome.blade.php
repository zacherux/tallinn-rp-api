<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Laravel</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Styles -->
    <link>
</head>
<body>
<div class="container">
    <div class="text-center">
        <h1 class="text-black-50 m-2">Laravel Api With Auth</h1>
    </div>
    @component('controllers.Auth')
    @endcomponent
</div>

</body>
</html>
