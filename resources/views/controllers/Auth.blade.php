<div>
    <div class="card">
        <div class="card-header" id="Auth-Controller"><strong class="text-black-50">Auth Controller</strong></div>
        <div class="card-body">
            <p>Auth controller is used to authenticate, register and getting users date</p>
            <div class="card-header font-weight-bold" id="/api/register">/api/register <span class="btn btn-success ml-5">POST</span></div>
            <div class="card-body">
                <div>
                    <p class="font-weight-bold">Input:</p>
                    <div class="border border-secondary p-2" >
                    <code>
                        body: {<br>
                            <div class="ml-2">
                                name: string|required,<br>
                                email: required|email|unique:users,<br>
                                password: required,<br>
                                c_password: required|same:password<br>
                            </div>
                        }<br>
                    </code>
                    </div>
                    <p class="border-top font-weight-bold mt-5 pt-4">Output:</p>
                    <div class="border border-secondary p-2" >
                        <code>
                            header: {<br>
                                <div class="ml-2 row">
                                    code: 200
                                </div>
                            },<br>
                            body: {<br>
                            <div class="ml-2">
                                success: bool,<br>
                                data: {<br>
                                <div class="ml-2">
                                    token: string,<br>
                                    name: string<br>
                                </div>
                                },<br>
                                message: string<br>
                            </div>
                            }<br>
                        </code>
                    </div>
                    <div class="border border-secondary mt-3 p-2" >
                        <code>
                            header: {<br>
                            <div class="ml-2 row">
                                code: 404
                            </div>
                            },<br>
                            body: {<br>
                            <div class="ml-2">
                                success: bool,<br>
                                data: string<br>
                                message: array (of errors)<br>
                            </div>
                            }<br>
                        </code>
                    </div>
                </div>
            </div>
            <div class="card-header font-weight-bold" id="/api/login">/api/login <span class="btn btn-success ml-5">POST</span></div>
            <div class="card-body">
                <div>
                    <p class="font-weight-bold">Input:</p>
                    <div class="border border-secondary p-2" >
                        <code>
                            body: {<br>
                            <div class="ml-2">
                                email: string,<br>
                                password: string,<br>
                            </div>
                            }<br>
                        </code>
                    </div>
                    <p class="border-top font-weight-bold mt-5 pt-4">Output:</p>
                    <div class="border border-secondary p-2" >
                        <code>
                            header: {<br>
                            <div class="ml-2 row">
                                code: 200
                            </div>
                            },<br>
                            body: {<br>
                            <div class="ml-2">
                                token: string,<br>
                                name: string
                            </div>
                            }<br>
                        </code>
                    </div>
                    <div class="border border-secondary mt-3 p-2" >
                        <code>
                            header: {<br>
                            <div class="ml-2 row">
                                code: 401
                            </div>
                            },<br>
                            body: {<br>
                            <div class="ml-2">
                                error: string<br>
                            </div>
                            }<br>
                        </code>
                    </div>
                </div>
            </div>
            <div class="card-header font-weight-bold" id="/api/getUserData">/api/getUserData <span class="btn btn-success ml-5">GET</span></div>
            <div class="card-body">
                <div>
                    <p class="font-weight-bold">Input:</p>
                    <div class="border border-secondary p-2" >
                        <code>
                            header: {<br>
                            <div class="ml-2">
                                Authorization: string
                            </div>
                            }<br>
                        </code>
                    </div>
                    <p class="border-top font-weight-bold mt-5 pt-4">Output:</p>
                    <div class="border border-secondary p-2" >
                        <code>
                            header: {<br>
                            <div class="ml-2 row">
                                code: 200
                            </div>
                            },<br>
                            body: {<br>
                            <div class="ml-2">
                                user: array,<br>
                                tokenData: array
                            </div>
                            }<br>
                        </code>
                    </div>
                    <div class="border border-secondary mt-3 p-2" >
                        <code>
                            header: {<br>
                            <div class="ml-2 row">
                                code: !200
                            </div>
                            },<br>
                            body: {<br>
                            <div class="ml-2">
                                error: string,<br>
                                fulleErrorMessage: string<br>
                            </div>
                            }<br>
                        </code>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
