<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGangApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gang_applications', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_identifier');
            $table->integer('web_user_id')->unsigned();
            $table->string('admin_web_user_id')->nullable();
            $table->string('gang_name');
            $table->json('answers');
            $table->string('rejection_reason')->nullable();
            $table->boolean('is_accepted')->default(false);
            $table->boolean('is_rejected')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('gang_applications');
    }
}
