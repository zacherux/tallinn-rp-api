<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class WantedCharacters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wanted_characters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_identifier');
            $table->string('character_name');
            $table->string('creator_id')->nullable();
            $table->string('creator_name')->nullable();
            $table->string('reason', 5012)->nullable();
            $table->string('image')->nullable();
            $table->boolean('is_wanted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wanted_characters');
    }
}
