<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeWhitelistCollation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('whitelist', function (Blueprint $table) {
            $table->string('identifier')->collation('utf8mb4_unicode_ci')->charset('utf8mb4')->change();
            $table->string('ban_reason')->collation('utf8mb4_unicode_ci')->charset('utf8mb4')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('whitelist', function (Blueprint $table) {
            $table->string('identifier')->collation('utf8mb4_general_ci')->change();
            $table->string('ban_reason')->collation('utf8mb4_general_ci')->change();
        });
    }
}
