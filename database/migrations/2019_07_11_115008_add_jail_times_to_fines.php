<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddJailTimesToFines extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('fine_types', function (Blueprint $table) {
            $table->integer('min_jail');
            $table->integer('max_jail');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('fine_types', function (Blueprint $table) {
            $table->dropColumn('min_jail');
            $table->dropColumn('max_jail');
        });
    }
}
