<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePriorityQueue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('priority_queues', function (Blueprint $table) {
            $table->dropColumn('steamId');
            $table->dropColumn('user_identifier');
            $table->integer('web_user_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('priority_queues', function (Blueprint $table) {
            $table->dropColumn('web_user_id');
            $table->mediumText('steamId');
            $table->mediumText('user_identifier');
        });
    }
}
