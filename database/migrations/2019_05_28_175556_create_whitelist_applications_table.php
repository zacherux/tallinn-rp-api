<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWhitelistApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('whitelist_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_identifier');
            $table->string('steamName');
            $table->string('characterName');
            $table->boolean('isAccepted');
            $table->boolean('isRejected');
            $table->boolean('is_chat')->default(false);
            $table->string('rejectionReason')->nullable();
            $table->string('web_user_id');
            $table->json('extra_questions');
            $table->string('admin_identifier')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('whitelist_applications');
    }
}
