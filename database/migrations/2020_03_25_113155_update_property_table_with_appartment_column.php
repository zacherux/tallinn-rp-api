<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePropertyTableWithAppartmentColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (filter_var(getenv('PROPERTIES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
            Schema::table('properties', function (Blueprint $table) {
                $table->boolean('is_apartment')->default(false);
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        if (filter_var(getenv('PROPERTIES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
            Schema::table('properties', function (Blueprint $table) {
                $table->dropColumn('is_apartment');
            });
        }
    }
}
