<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CriminalRecords extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('criminal_records', function (Blueprint $table) {
            $table->increments('id');
            $table->string('user_identifier');
            $table->string('character_name');
            $table->string('offence_type');
            $table->json('fine')->nullable();
            $table->string('jail_time')->nullable();
            $table->string('place')->nullable();
            $table->string('car_plate')->nullable();
            $table->string('other_parties')->nullable();
            $table->string('summary', 5012);
            $table->integer('police_officer_id');
            $table->boolean('warning_car');
            $table->boolean('warning_driver_license');
            $table->boolean('warning_gun_license');
            $table->boolean('is_criminal_case');
            $table->timestamp('criminal_case_expires_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('criminal_records');
    }
}
