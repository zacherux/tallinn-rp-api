<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AddRoles extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                'name' => 'admin',
                'description' => 'All powerful user',
            ]
        );
        DB::table('roles')->insert(
            [
                'name' => 'player',
                'description' => 'Regular user',
            ]
        );
        
        DB::table('roles')->insert(
            [
                'name' => 'mod',
                'description' => 'WebUser who has permission to see whitelist applications',
            ]
        );
    }
}
