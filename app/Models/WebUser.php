<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne as HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class WebUser extends Authenticatable
{
    use HasApiTokens, Notifiable;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'web_users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * @return BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    /**
     * @return BelongsToMany
     */
    public function fiveMUser()
    {

        return $this->belongsToMany(User::class);
    }

    /**
     * @return mixed
     */
    public function getFiveMUserData()
    {

        return $this->fiveMUser()->first();
    }

    /**
     * @return HasOne
     */
    public function whitelistApplication()
    {

        return $this->hasOne(WhitelistApplications::class);
    }

    /**
     * @return HasOne
     */
    public function userWebUser()
    {

        return $this->hasOne(UserWebUser::class, 'web_user_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function banReasons()
    {

        return $this->hasMany(WhitelistBanReason::class);
    }

    /**
     * @return HasOne
     */
    public function priority()
    {

        return $this->hasOne(PriorityQueue::class, 'web_user_id', 'id');
    }

    public function gangApplication()
    {

        return $this->hasMany(GangApplication::class);
    }

    public function adminGangApplication()
    {

        return $this->hasMany(GangApplication::class, 'admin_web_user_id');
    }

    /**
     * @param string|array $roles
     * @return bool
     */
    public function authorizeRoles($roles)

    {

        if (is_array($roles)) {

            return $this->hasAnyRole($roles) ||
                response()->json(
                    [
                        'error' => 'This action is unauthorized.'
                    ],
                    401
                );
        }

        return $this->hasRole($roles) ||
            response()->json(
                [
                    'error' => 'This action is unauthorized.'
                ],
                401
            );
    }

    /**
     * Check multiple roles
     *
     * @param array $roles
     * @return bool
     */
    public function hasAnyRole($roles)

    {

        return null !== $this->roles()->whereIn('name', $roles)->first();
    }

    /**
     * Check one role
     *
     * @param string $role
     * @return bool
     */
    public function hasRole($role)

    {

        return null !== $this->roles()->where('name', $role)->first();
    }
}
