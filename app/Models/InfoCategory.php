<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InfoCategory extends Model
{
    public function info()
    {

        return $this->hasMany(Info::class, 'info_categories_id', 'id');
    }
}
