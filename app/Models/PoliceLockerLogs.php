<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PoliceLockerLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'police_locker_logs';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function item()
    {
        return $this->hasOne(Item::class, 'name', 'itemName');
    }
}
