<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CarInsurance extends Model
{

    /**
     * @return BelongsTo
     */
    public function ownedVehicle(): BelongsTo
    {

        return $this->belongsTo(OwnedCar::class, 'plate', 'plate');
    }
}
