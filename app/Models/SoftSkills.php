<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SoftSkills extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'softskills';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * @return void
     */
    public function user()
    {

        return $this->belongsTo(User::class, 'identifier', 'identifier');
    }
}
