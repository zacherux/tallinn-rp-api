<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RulesCategory extends Model
{

    public function rules() {
        return $this->hasMany(Rules::class, 'rules_category_id', 'id');
    }
}
