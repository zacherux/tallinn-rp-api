<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Whitelist extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'whitelist';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'identifier';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function userWebUser()
    {

        return $this->belongsTo(UserWebUser::class, 'steam_hex', 'identifier');
    }
}
