<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'items';

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'name';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function ambulanceLockerLogs()
    {
        return $this->belongsToMany(AmbulanceLockerLogs::class, 'itemName', 'name');
    }
}
