<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WantedCar extends Model
{
    public function ownedCar()
    {
        return $this->hasOne(OwnedCar::class, 'plate', 'plate');
    }
}
