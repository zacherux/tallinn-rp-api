<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Job extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'jobs';

    public function jobGrade() {
        return $this->hasMany(JobGrades::class, 'job_name', 'name');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'job', 'name');
    }
}
