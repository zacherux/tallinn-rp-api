<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class WantedCharacters
 */
class WantedCharacters extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }

    public function officer()
    {

        return $this->hasOne(User::class, 'identifier', 'creator_id');
    }
}
