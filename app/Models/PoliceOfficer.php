<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;

/**
 * Class PoliceOfficer
 */
class PoliceOfficer extends Model
{

    /**
     * @return BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function criminalRecord()
    {

        return $this->hasMany(CriminalRecord::class);
    }
}
