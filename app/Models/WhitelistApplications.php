<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhitelistApplications extends Model
{

    public function webUser()
    {
        return $this->belongsTo(WebUser::class);
    }

    public function user()
    {

        return $this->belongsTo(User::class);
    }

    public function admin()
    {
        return $this->belongsTo(WebUser::class, 'admin_identifier', 'id');
    }
}
