<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserLicenses extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_licenses';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function user() {
        return $this->belongsTo(User::class, 'identifier');
    }
}
