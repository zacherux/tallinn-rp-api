<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'properties';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function ownedProperties() {
        return $this->hasMany(OwnedProperty::class, 'name', 'name');
    }
}
