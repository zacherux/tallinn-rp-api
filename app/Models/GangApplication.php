<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GangApplication extends Model
{

    public function character()
    {

        return $this->belongsTo(User::class, 'user_identifier', 'identifier');
    }

    public function webUser()
    {

        return $this->belongsTo(WebUser::class, 'web_user_id', 'id');
    }

    public function adminWebUser()
    {
        return $this->belongsTo(WebUser::class, 'web_user_id', 'id');
    }
}
