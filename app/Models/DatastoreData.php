<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DatastoreData extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'datastore_data';

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function user() {
        return $this->belongsTo(User::class, 'owner', 'identifier');
    }
}
