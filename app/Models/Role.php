<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Role Model
 */
class Role extends Model
{

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function webUsers()
    {

        return $this->belongsToMany(WebUser::class);
    }
}
