<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo as BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;

/**
 * Class EmsWorker
 */
class EmsWorker extends Model
{

    /**
     * @return BelongsTo
     */
    public function user()
    {

        return $this->belongsTo(User::class);
    }

    /**
     * @return HasMany
     */
    public function medicalHistory()
    {

        return $this->hasMany(MedicalHistory::class);
    }
}
