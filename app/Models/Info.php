<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Info extends Model
{

    public function category()
    {
        return $this->belongsTo(InfoCategory::class, 'info_categories_id', 'id');
    }
}
