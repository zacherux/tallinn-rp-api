<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dpkeybind extends Model
{

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    public function user() {
        return $this->belongsTo(User::class, 'id', 'identifier');
    }
}
