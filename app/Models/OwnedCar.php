<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OwnedCar extends Model
{

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'plate';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @var bool
     */
    public $timestamps = false;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owned_vehicles';

    /**
     * @return void
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'owner', 'identifier');
    }

    /**
     * @return void
     */
    public function insurance()
    {

        return $this->hasOne(CarInsurance::class, 'plate', 'plate');
    }

    /**
     * @return void
     */
    public function stolenCar()
    {
        return $this->hasOne(StolenCar::class, 'plate', 'plate');
    }

    /**
     * @return void
     */
    public function wantedCar()
    {
        return $this->hasOne(WantedCar::class, 'plate', 'plate');
    }
}
