<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PriorityQueue extends Model
{

    public function webUser()
    {
        return $this->belongsTo(WebUser::class, 'web_user_id', 'id');
    }
}
