<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Rules extends Model
{

    public function category()
    {

        return $this->belongsTo(RulesCategory::class, 'rules_category_id', 'id');
    }
}
