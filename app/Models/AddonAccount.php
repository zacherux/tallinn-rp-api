<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AddonAccount extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'addon_account';
}
