<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AmbulanceLockerLogs extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ambulance_locker_logs';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function item()
    {
        return $this->hasOne(Item::class, 'name', 'itemName');
    }
}
