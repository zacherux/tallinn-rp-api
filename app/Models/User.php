<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany as BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany as HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne as HasOne;

class User extends Model
{

    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'identifier';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * @return BelongsToMany
     */
    public function webUser(): BelongsToMany
    {

        if(!env('HEX_IDENTIFIER')) {
            return $this->belongsToMany(WebUser::class);
        }

        return  $this->belongsToMany(WebUser::class, null,'steam_hex', 'identifier');
    }

    /**
     * @return HasMany
     */
    public function criminalRecords(): HasMany
    {

        return $this->hasMany(CriminalRecord::class);
    }

    /**
     * @return HasMany
     */
    public function medicalHistory(): HasMany
    {

        return $this->hasMany(MedicalHistory::class);
    }

    /**
     * @return HasOne
     */
    public function wantedCharacter(): HasOne
    {

        return $this->hasOne(WantedCharacters::class);
    }

    /**
     * @return HasMany
     */
    public function jobApplication(): HasMany
    {

        return $this->hasMany(JobApplication::class);
    }

    /**
     * @return HasOne
     */
    public function policeOfficer(): HasOne
    {

        return $this->hasOne(PoliceOfficer::class);
    }

    /**
     * @return HasOne
     */
    public function emsWorker(): HasOne
    {

        return $this->hasOne(EmsWorker::class);
    }

    /**
     * @return HasMany
     */
    public function mechanicReports(): HasMany
    {

        return $this->hasMany(MechanicReports::class);
    }

    /**
     * @return HasMany
     */
    public function cars(): HasMany
    {
        return $this->hasMany(OwnedCar::class, 'owner');
    }

    /**
     * @return HasMany
     */
    public function licenses(): HasMany
    {
        return $this->hasMany(UserLicenses::class, 'owner');
    }

    /**
     * @return HasMany
     */
    public function bills(): HasMany
    {
        return $this->hasMany(Bill::class, 'identifier');
    }

    /**
     * @return HasMany
     */
    public function ownedProperties(): HasMany
    {
        return $this->hasMany(OwnedProperty::class, 'owner');
    }

    /**
     * @return HasMany
     */
    public function banReasons(): HasMany
    {
        return $this->hasMany(WhitelistBanReason::class);
    }

    /**
     * @return HasOne
     */
    public function keybinds(): HasOne
    {
        return $this->hasOne(Dpkeybind::class, 'id', 'identifier');
    }

    /**
     * @return HasMany
     */
    public function dataStoreData(): HasMany
    {
        return $this->hasMany(DatastoreData::class, 'identifier', 'owner');
    }

    /**
     * @return HasOne
     */
    public function job(): HasOne
    {
        return $this->hasOne(Job::class, 'name', 'job');
    }

    /**
     * @return HasMany
     */
    public function softSkills(): HasMany
    {

        return $this->hasMany(SoftSkills::class, 'identifier', 'identifier');
    }

    /**
     * @return HasMany
     */
    public function gangApplication(): HasMany
    {

        return $this->hasMany(GangApplication::class, 'user_identifier', 'identifier');
    }
}
