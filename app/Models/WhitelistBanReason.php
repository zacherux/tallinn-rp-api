<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class WhitelistBanReason extends Model
{

    public function webUser()
    {
        return $this->belongsTo(WebUser::class);
    }
}
