<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;

class UserWebUser extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_web_user';

    /**
     * @return BelongsTo
     */
    public function webUser() : BelongsTo
    {
        return $this->belongsTo(WebUser::class);
    }

    /**
     * @return BelongsTo
     */
    public function character() : BelongsTo
    {

        if(!env('HEX_IDENTIFIER')) {
            return $this->belongsTo(User::class);
        }

        return $this->belongsTo(User::class, 'identifier', 'steam_hex');
    }

    /**
     * @return HasOne
     */
    public function whitelist() : HasOne
    {
        return $this->hasOne(Whitelist::class, 'identifier', 'steam_hex');
    }
}
