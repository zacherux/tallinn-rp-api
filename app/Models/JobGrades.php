<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class JobGrades extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'job_grades';

    public function job() {
        return $this->belongsTo(Job::class, 'name', 'job_name');
    }
}
