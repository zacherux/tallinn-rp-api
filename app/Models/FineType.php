<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FineType extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'fine_types';

    /**
     * @var bool
     */
    public $timestamps = false;
}
