<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OwnedProperty extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owned_properties';

    /**
     * @var bool
     */
    public $timestamps = false;

    public function property() {
        return $this->hasOne(Property::class, 'name', 'name');
    }

    public function user() {
        return $this->hasOne(User::class, 'identifier', 'owner');
    }
}
