<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OwnedMotel extends Model
{

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'owned_motell';
}
