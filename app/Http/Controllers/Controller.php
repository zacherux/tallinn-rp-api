<?php

namespace App\Http\Controllers;

use App\WebUser;
use Illuminate\Http\Request;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Exception;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function returnError(Exception $e)
    {
        return response()->json(
            [
                'error' => 'There was some uexpected error',
                'fulleErrorMessage' => $e->getMessage()
            ],
            (is_numeric($e->getCode()) and $e->getCode() !== 0 and $e->getCode() < 600) ? $e->getCode() : 500
        );
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(
            [
                'roles',
                'fiveMUser'
            ]
        )->first();
    }
}
