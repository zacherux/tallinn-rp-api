<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Info;
use App\Models\InfoCategory;
use App\Models\Rules;
use App\Models\RulesCategory;
use Exception;
use Validator;

class RulesController extends Controller
{

    public function getAllRules()
    {

        try {
            return response()->json(
                [
                    'rules' => Rules::with('category')->get()
                ]
            );
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getRuleCategories()
    {

        try {
            return response()->json(
                [
                    'rules' => RulesCategory::with('rules')->get()
                ]
            );
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getAllInfo()
    {

        try {
            return response()->json(
                [
                    'info' => Info::with('category')->get()
                ]
            );
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getInfoCategories()
    {

        try {
            return response()->json(
                [
                    'info' => InfoCategory::with('info')->get()
                ]
            );
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
