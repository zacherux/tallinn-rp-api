<?php

namespace App\Http\Controllers\API;

use App\Models\AddonAccount;
use Exception;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class SociatyController extends Controller
{


    public function acounts(Request $request)
    {

        try {
            return response()->json(AddonAccount::all(['name']));
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
