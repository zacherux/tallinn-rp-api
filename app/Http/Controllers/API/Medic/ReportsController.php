<?php

namespace App\Http\Controllers\API\Medic;

use App\Models\EmsWorker;
use App\Http\Controllers\Controller;
use App\Models\MedicalHistory;
use App\Models\User;
use App\Models\WebUser;
use Illuminate\Http\Request;
use Exception;
use Validator;

class ReportsController extends Controller
{

    const ALLOWED_JOBS = ['ambulance', 'offambulance'];

    public function createReport(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'summary' => 'required',
                'patiendId' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            if (!$user->fivemUser[0]->emsWorker) {
                $emsWorker = new EmsWorker();
                $emsWorker->user_identifier = $user->fivemUser[0]->identifier;
                $emsWorker->is_hired = true;
                $emsWorker->save();
                $user = $this->getLogedInUserData($request);
            }

            $patient = User::where('identifier', $request->patiendId)->first();

            $mecialReport = New MedicalHistory();
            $mecialReport->user_identifier = $patient->identifier;
            $mecialReport->characterName = $patient->firstname . ' ' . $patient->lastname;
            $mecialReport->summary = $request->summary;
            $mecialReport->ems_worker_id = $user->fivemUser[0]->emsWorker->id;
            $mecialReport->save();

            return response()->json();

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function deleteRaport(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $medicalRecord = MedicalHistory::where('id', $request->id)->delete();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser', 'fiveMUser.emsWorker'])->first();
    }
}
