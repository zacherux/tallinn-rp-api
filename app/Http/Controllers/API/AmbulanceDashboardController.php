<?php

namespace App\Http\Controllers\API;

use App\Models\Bill;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JobApplication;
use App\Models\MedicalHistory;
use App\Models\User;
use App\Models\WebUser;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class AmbulanceDashboardController extends Controller
{

    const ALLOWED_JOBS = ['ambulance', 'offambulance'];

    public function getTopDebtors()
    {

        try{
            $bills = Bill::where('target', 'society_ambulance')->get();
            $collection = [];
            foreach($bills as $bill) {
                if(!array_key_exists($bill->identifier, $collection)) {
                    $user = User::where('identifier', $bill->identifier)->first();
                    $collection[$bill->identifier] = [
                        'name' => $user->firstname . ' ' . $user->lastname,
                        'sum' => $bill->amount
                    ];
                } else {
                    $collection[$bill->identifier]['sum'] = $collection[$bill->identifier]['sum'] + $bill->amount;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['deptors' => $collection->sortByDesc('sum')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getTopWorkers()
    {

        try{
            $records = MedicalHistory::where('created_at',  '>', Carbon::now()->subWeek())->with(['emsWorker.user'])->orderBy('id', 'DESC')->get();;
            $collection = [];
            foreach($records as $record) {
                if(!array_key_exists($record->emsWorker->user_identifier, $collection)) {
                    $collection[$record->emsWorker->user_identifier] = [
                        'name' => $record->emsWorker->user->firstname . ' ' . $record->emsWorker->user->lastname,
                        'count' => 1
                    ];
                } else {
                    $collection[$record->emsWorker->user_identifier]['count']++;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['workers' => $collection->sortByDesc('count')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getAllWorkers(Request $request)
    {

        try{
            $user = $this->getLogedInUserData($request);
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            return response()->json(
                [
                    'workers' => User::where('job', 'ambualnce')->orWhere('job', 'offambulance')->get()
                ]
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function fireWorker(Request $request)
    {

        try{
            $user = $this->getLogedInUserData($request);
            if(!$user->hasAnyRole(['admin', 'mod']) and (!in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS) or $user->fivemUser[0]->job_grade < 90)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'workerHex' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 420);
            }

            $user = User::where('identifier', $request->workerHex)->first();
            $user->job = 'unemployed';
            $user->job_grade = 0;
            $user->save();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getJobAppliactionsCounts()
    {

        try{
            $jobApplications = JobApplication::where('job', 'ems')->get();
            return response()->json([
                'total' => $jobApplications->count(),
                'unchecked' => $jobApplications->where('isAccepted', false)->where('isRejected', false)->where('initiated', false)->count(),
                'waiting' => $jobApplications->where('isAccepted', true)->where('isRejected', false)->where('initiated', false)->count(),
            ]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
    }
}
