<?php

namespace App\Http\Controllers\API\Logs;

use App\Models\AmbulanceLockerLogs;
use App\Http\Controllers\Controller;
use App\Models\MechanicLockerLogs;
use App\Models\PoliceLockerLogs;
use Illuminate\Http\Request;
use Exception;
use Validator;

class LogsController extends Controller
{

    public function getLogs(Request $request)
    {
        try {
            $request->user()->authorizeRoles('admin', 'mod');

            $job = $request->job;
            if ($job == 'police') {
                $logs = PoliceLockerLogs::with(['item'])->orderBy('dateTime', 'DESC');
            } elseif ($job == 'mechanic') {
                $logs = MechanicLockerLogs::with(['item'])->orderBy('dateTime', 'DESC');
            } elseif ($job == 'ambulance') {
                $logs = AmbulanceLockerLogs::with(['item'])->orderBy('dateTime', 'DESC');
            }

            if ($request->keyword) {
                $logs->where('name', 'like', '%' . $request->keyword . '%')
                    ->orWhere('itemName', 'like', '%' . $request->keyword . '%')
                    ->orWhere('dateTime', 'like', '%' . $request->keyword . '%')
                    ->orWhere('action', 'like', '%' . $request->keyword . '%')
                    ->orWhere('count', $request->keyword);
            }


            return response()->json($logs->limit(100)->get());
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0 and $e->getCode() > 500) ? 500 : 500
            );
        }
    }
}
