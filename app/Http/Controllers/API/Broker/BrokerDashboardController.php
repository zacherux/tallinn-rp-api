<?php

namespace App\Http\Controllers\API\Broker;

use Exception;
use App\Http\Controllers\Controller;
use App\Models\Property;
use Illuminate\Support\Collection;

class BrokerDashboardController extends Controller
{

    public function getAvailablePropertiesForDashboard()
    {

        try{
            $properties = Property::with('ownedProperties', 'ownedProperties.user')->get();
            $freeProperties = array_merge($properties->where('ownedProperties', '[]')->toArray(), $properties->where('is_apartment', true)->toArray());
            $freeProperties = new Collection($freeProperties);
            return response()->json(['properties' => $freeProperties->shuffle()->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getAvailableProperties()
    {

        try{
            $properties =  Property::with('ownedProperties', 'ownedProperties.user')->get();
            return response()->json(
                [
                    'properties' => array_merge(
                        $properties->where('ownedProperties', '[]')->toArray(),
                        $properties->where('is_apartment', true)->toArray()
                    )
                ]
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getAllPropertiesForDashboard()
    {

        try{
            $properties = Property::with('ownedProperties', 'ownedProperties.user')->inRandomOrder()->limit(5)->get();
            return response()->json(['properties' => $properties]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getAllProperties()
    {

        try{
            $properties = Property::with('ownedProperties', 'ownedProperties.user')->orderBy('price')->get();
            return response()->json(['properties' => $properties]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
