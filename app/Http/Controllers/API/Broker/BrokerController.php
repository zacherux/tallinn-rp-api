<?php

namespace App\Http\Controllers\API\Broker;

use Exception;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OwnedProperty;
use App\Models\Property;
use App\Models\WebUser;
use Validator;

class BrokerController extends Controller
{

    const ALLOWED_JOBS = ['realestateagent'];

    public function getProperty(Request $request)
    {
        try{
            $property = Property::where('id', $request->id)->with('ownedProperties', 'ownedProperties.user')->first();
            return response()->json(['property' => $property]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function changePropertyPrice(Request $request)
    {

        try{
            $logedInUserJob = $this->getFivemUserData($request)->job;

            if (!in_array($logedInUserJob, self::ALLOWED_JOBS) and !$request->user()->hasAnyRole(['admin', 'mod'])) {
                return response()->json('Unauthorized', 401);
            }
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'price' => 'required',
            ]);
            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $property = Property::where('id', $request->id)->first();
            $property->price = $request->price;
            $property->save();

            $ownedProperties = OwnedProperty::where('name', $property->name)->get();
            if (count($ownedProperties) > 0) {
                foreach ($ownedProperties as $ownedProperty) {
                    if ($ownedProperty->is_owner) {
                        $ownedProperty->price = $request->price;
                        $ownedProperty->save();
                    }
                }
            }
            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function buyBackProperty(Request $request)
    {
        try {
            $logedInUserJob = $this->getFivemUserData($request)->job;

            if (!in_array($logedInUserJob, self::ALLOWED_JOBS) and !$request->user()->hasAnyRole(['admin', 'mod'])) {
                return response()->json('Unauthorized', 401);
            }
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $property = Property::where('id', $request->id)->first();
            if($request->owner) {
                $ownedProperties = OwnedProperty::where('name', $property->name)->where('owner', $request->owner)->delete();
            } else {
                $ownedProperties = OwnedProperty::where('name', $property->name)->delete();
            }

            return response()->json();

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function sellProperty(Request $request)
    {

        try {
            $logedInUserJob = $this->getFivemUserData($request)->job;

            if (!in_array($logedInUserJob, self::ALLOWED_JOBS) and !$request->user()->hasAnyRole(['admin', 'mod'])) {
                return response()->json('Unauthorized', 401);
            }
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'newOwner' => 'required'
            ]);
            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $property = Property::where('id', $request->id)->first();
            if($request->oldOwner) {
                // re-sell apartment that is already owned
                // in same apartment house you can have several apartmets owned by different people
                $ownedProperties = OwnedProperty::where('name', $property->name)->where('owner', $request->oldOwner)->first();
                $ownedProperties->owner = $request->newOwner;
                $ownedProperties->save();
            } else {
                //sell new apartment or house
                $ownedProperties = OwnedProperty::where('name', $property->name)->get();
                if (count($ownedProperties) > 0 && !$property->is_apartment) {
                    foreach($ownedProperties as $key => $ownedProperty) {
                        if ($key === 0) {
                            $ownedProperty->owner = $request->newOwner;
                            $ownedProperty->save();
                        } else {
                            $ownedProperty->delete();
                        }
                    }
                } else {
                    $newOwnedProperty = new OwnedProperty();
                    $newOwnedProperty->name = $property->name;
                    $newOwnedProperty->price = $property->price;
                    $newOwnedProperty->rented = 0;
                    $newOwnedProperty->owner = $request->newOwner;
                    $newOwnedProperty->save();
                }
            }

            return response()->json();

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function giveKey(Request $request) {
        try {
            $logedInUserJob = $this->getFivemUserData($request)->job;

            if (!in_array($logedInUserJob, self::ALLOWED_JOBS) and !$request->user()->hasAnyRole(['admin', 'mod'])) {
                return response()->json('Unauthorized', 401);
            }
            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'newKeyOwner' => 'required'
            ]);
            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }
            $property = Property::where('id', $request->id)->first();
            $ownedProperty = new OwnedProperty();
            $ownedProperty->name = $property->name;
            $ownedProperty->price = 1;
            $ownedProperty->rented = false;
            $ownedProperty->owner = $request->newKeyOwner;
            $ownedProperty->is_owner = false;
            $ownedProperty->save();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function removeKey(Request $request) {
        try {
            $logedInUserJob = $this->getFivemUserData($request)->job;

            if (!in_array($logedInUserJob, self::ALLOWED_JOBS) and !$request->user()->hasAnyRole(['admin', 'mod'])) {
                return response()->json('Unauthorized', 401);
            }
            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);
            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }
            $ownedProperty = OwnedProperty::where('id', $request->id)->delete();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    protected function getWebUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
    }
     /**
     * Undocumented function
     *
     * @param Request $request
     * @return string
     */
    protected function getFivemUserData(Request $request)
    {

        return $this->getWebUserData($request)->fivemUser[0];
    }
}
