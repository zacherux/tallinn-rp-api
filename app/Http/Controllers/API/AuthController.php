<?php

namespace App\Http\Controllers\API;

use App\Models\Role;
use Exception;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\WebUser;
use App\Models\Whitelist;
use App\Models\WhitelistApplications;
use Illuminate\Support\Facades\Log;
use Validator;

class AuthController extends Controller
{

    /**
     * Register api
     * @param Request $request
     * @return JsonResponse
     */
    public function register(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:web_users',
            'password' => 'required',
            'c_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        try {
            $user = new WebUser();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            $user->roles()->attach(Role::where('name', 'player')->first());
        } catch (Exception $exception) {
            Log::info('user creation failed');
            Log::info('error message', [$exception->getMessage()]);
            Log::info('error code', [$exception->getCode()]);
            return response()->json(
                [
                    'errorMessages' => (isset($exception->getMessage()->message)) ? $exception->getMessage()->message : 'Ups, We did not seen that one coming',
                    'errorCode' => ($exception->getCode()) ? $exception->getCode() : 500
                ],
                ($exception->getCode() and is_int($exception->getCode())) ? $exception->getCode() : 500
            );
        }
        $success['token'] = $user->createToken(getenv('TOKEN_NAME'))->accessToken;
        $success['name'] = $user->name;

        $response = [
            'success' => true,
            'data' => $success,
            'message' => 'User register successfully.'
        ];

        return response()->json($response, 200);
    }

    /**
     * Create whitelist application and create new user
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function createWhitelistApplication(Request $request): JsonResponse
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:web_users',
            'password' => 'required',
            'c_password' => 'required|same:password',
            'characterName' => 'required',
            'steamName' => 'required',
            'steamHex' => 'required',
            'questions' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        //chekc unique steam hex. steam hex in database has brefix but user does not have to add brefix
        $steamId = 'steam:' . strtolower($request->steamHex);
        if (Whitelist::where('identifier', $steamId)->orWhere('identifier', 'ban:' . $request->steamHex)->first()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => [
                    'steamHex' => ['Hex already exist. Use another one']
                ]
            ];
            return response()->json($response, 405);
        }

        try {
            // create user
            $user = new WebUser();
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = bcrypt($request->password);
            $user->save();

            //set role
            $user->roles()->attach(Role::where('name', 'player')->first());

            // create whitelist application
            $wlApp = new WhitelistApplications();
            $wlApp->user_identifier = $steamId;
            $wlApp->steamName = $request->steamName;
            $wlApp->characterName = $request->characterName;
            $wlApp->isAccepted = 0;
            $wlApp->isRejected = 0;
            $wlApp->rejectionReason = null;
            $wlApp->extra_questions = json_encode($request->questions);
            $wlApp->web_user_id = $user->id;
            $wlApp->save();

            $token = $user->createToken(getenv('TOKEN_NAME'));
            $userRole = WebUser::where('id', $user->id)->with(['roles'])->first()->roles[0];

            return response()->json(
                [
                    'token' => $token->accessToken,
                    'name' => $user->name,
                    'userId' => $user->id,
                    'expiresAt' => $token->token->expires_at,
                    'roleId' => $userRole->id,
                    'roleName' => $userRole->name
                ],
                200
            );
        } catch (Exception $exception) {
            $response = [
                'success' => false,
                'data' => 'Server error.',
                'messages' => $exception->getMessage()
            ];
            return response()->json($response, 500);
        }
    }

    /**
     * Login api
     */
    public function login(): JsonResponse
    {
        if (Auth::attempt(['email' => request('email'), 'password' => request('password')])) {
            $user = Auth::user();
            $token = $user->createToken(getenv('TOKEN_NAME'));
            $name = $user->name;
            $userRole = WebUser::where('id', $user->id)->with(['roles'])->first()->roles[0];

            return response()->json(
                [
                    'token' => $token->accessToken,
                    'name' => $name,
                    'userId' => $user->id,
                    'expiresAt' => $token->token->expires_at,
                    'roleId' => $userRole->id,
                    'roleName' => $userRole->name
                ],
                200
            );
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
}
