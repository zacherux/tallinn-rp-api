<?php

namespace App\Http\Controllers\API;

use App\Models\CarModel;
use App\Models\House;
use Exception;
use App\Models\WebUser;
use Illuminate\Http\Request;
use App\Http\Controllers\API\Helper\UserBaseController;
use App\Models\Job;
use App\Models\JobGrades;
use App\Models\User;

class LogedInUserCharactersController extends UserBaseController
{

    private $houses = null;

    public function getCharacters(Request $request)
    {
        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['userWebUser'])->first();

            if (!$user->userWebUser->user_identifier) {
                return response()->json('no characters found', 404);
            }

            $isSkills = ($request->isSkills != null) ? filter_var(
                $request->isSkills,
                FILTER_VALIDATE_BOOLEAN,
                FILTER_NULL_ON_FAILURE
            ) : false;

            if (!env('HEX_IDENTIFIER')) {
                $croppedIdentifier = substr(
                    $user->userWebUser->user_identifier,
                    strpos($user->userWebUser->user_identifier, ":") + 1
                );
            } else {
                $croppedIdentifier = substr(
                    $user->userWebUser->steam_hex,
                    strpos($user->userWebUser->user_identifier, ":") + 1
                );
            }

            $characters = User::where('identifier', 'LIKE', '%' . $croppedIdentifier)->with(
                $this->getRelations($isSkills)
            )->orderBy('identifier', 'DESC')->get();

            foreach ($characters as $character) {
                $jobFrades = JobGrades::where('job_name', $character->job)->where(
                    'grade',
                    $character->job_grade
                )->first();

                $character->modifiedLicenses = $this->getLicenses($character->licenses);
                $character->jobLabel = Job::where('name', $character->job)->first()->label;
                $character->jobGradeLabel = $jobFrades->label;
                $character->salary = $jobFrades->salary;

                $this->modifieCarsData($character->cars);
                if (filter_var(getenv('HOUSES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
                    $character->house = $this->modifieHouses(
                        $croppedIdentifier,
                        $character->firstname . ' ' . $character->lastname
                    );
                }
                $character->criminalRecordsCount = $this->getCriminalRecordsCount($character->criminalRecords);
                $character->medicalCasesCount = $this->getMedicalRecordsCount($character->medicalHistory);
            }

            return response()->json($characters);
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    private function getRelations($isWithSkills)
    {
        $withRelations = [
            'criminalRecords',
            'criminalRecords.policeOfficer',
            'criminalRecords.policeOfficer.user',
            'medicalHistory',
            'medicalHistory.emsWorker',
            'medicalHistory.emsWorker.user',
            'cars',
            'cars.insurance',
            'cars.stolenCar',
            'cars.wantedCar',
            'wantedCharacter',
            'wantedCharacter.officer',
            'licenses',
            'bills',
            'jobApplication',
            'gangApplication'
        ];

        if (filter_var(getenv('PROPERTIES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
            $withRelations[] = 'ownedProperties';
            $withRelations[] = 'ownedProperties.property';
        }

        if ($isWithSkills) {
            $withRelations[] = 'softSkills';
        }

        return $withRelations;
    }

    private function modifieCarsData($cars)
    {
        $outsideGarage = 0;
        $total = 0;
        foreach ($cars as $car) {
            $vehicleData = json_decode($car->vehicle);
            $carModel = CarModel::where('code', $vehicleData->model)->first();
            if ($carModel) {
                $car->name = $carModel->name;
            } else {
                $car->name = '';
            }

            if ($car->stored) {
                $outsideGarage++;
            }
            $total++;
            unset($car->color);
            unset($car->financetimer);
            unset($car->financetimer);
            unset($car->fourrieremecano);
            unset($car->garage);
            unset($car->insurance);
            unset($car->jamstate);
            unset($car->lasthouse);
            unset($car->state);
            unset($car->stolen_car);
            unset($car->stored);
            unset($car->vehicle);
            unset($car->vehiclename);
            unset($car->wanted_car);
        }

        $cars['counts'] = [
            'total' => $total,
            'outsideGarage' => $outsideGarage
        ];
    }

    public function modifieHouses($croppedLicence, $characterName)
    {
        if (!$this->houses || !array_keys($this->houses, $croppedLicence)) {
            $this->houses = [$croppedLicence => House::where('owner', 'like', '%' . $croppedLicence)->get()];
        }

        $houses = [];
        foreach ($this->houses[$croppedLicence] as $house) {
            unset($house->entry);
            unset($house->garage);
            unset($house->housekeys);
            unset($house->interior);
            unset($house->inventory);
            unset($house->inventorylocation);
            unset($house->last_repayment);
            unset($house->mortgage_owed);
            unset($house->resalejob);
            unset($house->resalepercent);
            unset($house->shells);
            unset($house->shell);
            unset($house->wardrobe);
            unset($house->doors);
            unset($house->furniture);
            unset($house->owned);
            if ($house->ownername == $characterName) {
                $houses[] = $house;
            }
        }

        return $houses;
    }
}
