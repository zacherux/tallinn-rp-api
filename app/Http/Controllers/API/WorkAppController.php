<?php

namespace App\Http\Controllers\API;

use Exception;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\JobApplication;
use App\Models\WebUser;
use Validator;

class WorkAppController extends Controller
{

    const ALLOWED_JOB_TO_SEE_WORK_APPLICTION = ['police', 'offpolice', 'ambulance', 'ambulance'];

    public function save(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'identifier' => 'required',
                'questions' => 'required',
                'job' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $workApplication = new JobApplication();
            $workApplication->user_identifier = $request->identifier;
            $workApplication->web_user_id = $request->user()->token()->user_id;
            $workApplication->answers = json_encode($request->questions);
            $workApplication->job = $request->job;
            $workApplication->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getMyWorkApplications(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['jobApplication'])->first();

            return response()->json($user->jobApplication);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getWorkApplications(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOB_TO_SEE_WORK_APPLICTION)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'work' => 'required',
                'accepted' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $workApplications = JobApplication::where('job', $request->work)
                ->where('isAccepted', filter_var($request->accepted, FILTER_VALIDATE_BOOLEAN))
                ->where('isRejected', false)
                ->where('initiated', false)
                ->with('user', 'user.job')
                ->get();

            return response()->json($workApplications);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function accept(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOB_TO_SEE_WORK_APPLICTION)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $workApplication = JobApplication::where('id', $request->id)->first();
            $workApplication->isRejected = false;
            $workApplication->bossIdentifier = $user->fivemUser[0]->identifier;

            if ($workApplication->isAccepted) {
                $workApplication->initiated = true;
            } else {
                $workApplication->isAccepted = true;
            }
            $workApplication->save();
            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function reject(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOB_TO_SEE_WORK_APPLICTION)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $workApplication = JobApplication::where('id', $request->id)->first();
            $workApplication->isAccepted = false;
            $workApplication->isRejected = true;
            $workApplication->initiated = false;
            $workApplication->bossIdentifier = $user->fivemUser[0]->identifier;
            $workApplication->save();

            return response()->json('test');
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(
            [
                'roles',
                'fiveMUser',
            ]
        )->first();
    }
}
