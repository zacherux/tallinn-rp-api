<?php

namespace App\Http\Controllers\API;

use App\Models\CarModel;
use App\Models\CarSellRequest;
use Exception;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\OwnedCar;
use Illuminate\Support\Collection;

class ArkDashboardController extends Controller
{

    public function getTopCarModels()
    {

        try{
            $cars = OwnedCar::all();
            $collection = [];
            foreach($cars as $car) {
                $metaData = json_decode($car->vehicle);
                $modelNumber = $metaData->model;
                if(!array_key_exists('modelNr' . $modelNumber, $collection)) {
                    $carModel = CarModel::where('code', $modelNumber)->first();
                    if ($carModel) {
                        $collection['modelNr' . $modelNumber] = [
                            'name' => $carModel->name,
                            'count' => 1
                        ];
                    }
                } else {
                    $collection['modelNr' . $modelNumber]['count']++;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['models' => $collection->sortByDesc('count')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getSellRequestCounts()
    {

        try{
            $sellRequst = CarSellRequest::all();
            return response()->json(
                [
                    'total' => $sellRequst->count(),
                    'waiting' => $sellRequst->where('confirmedBy', false)->count()
                ]
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
