<?php

namespace App\Http\Controllers\API;

use App\Models\CarInsurance;
use App\Models\CarModel;
use App\Models\CarSellLogs;
use App\Models\ConfiscateCarLog;
use App\Models\User;
use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OwnedCar;
use App\Models\StolenCar;
use App\Models\WantedCar;
use App\Models\WebUser;
use Carbon\Carbon;

class CarsController extends Controller
{

    const ALLOWED_JOBS = [
        'police' => ['police', 'offpolice'],
        'mechanic' => ['deluxe', 'offdeluxe'],
    ];

    public function changeModelName(Request $request)
    {
        try {
            if (!$this->isJobPoliceAndMechanic($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
                'name' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $car = OwnedCar::where('plate', $request->plate)->first();
            $vehicleData = json_decode($car->vehicle);
            $model = CarModel::where('code', $vehicleData->model)->first();
            if ($model) {
                $model->name = $request->name;
                $model->save();
            } else {
                $model = new CarModel();
                $model->name = $request->name;
                $model->code = $vehicleData->model;
                $model->save();
            }
            return response()->json([]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function addInsurance(Request $request)
    {

        try {
            if (!$this->isJobPolice($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $carInsurance = CarInsurance::where('plate', $request->plate)->first();
            if ($carInsurance) {
                $carInsurance->end_time = (new Carbon())->addMonth();
                $carInsurance->save();
            } else {
                $carInsurance = new CarInsurance();
                $carInsurance->plate = $request->plate;
                $carInsurance->is_insured = true;
                $carInsurance->end_time = (new Carbon())->addMonth();
                $carInsurance->save();
            }
            return response()->json([]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function addCarAsStolen(Request $request)
    {

        try {
            if (!$this->isJobPolice($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $stolenCar = StolenCar::where('plate', $request->plate)->first();
            if ($stolenCar) {
                $stolenCar->is_stolen = true;
                $stolenCar->description = $request->description;
                $stolenCar->save();
            } else {
                $stolenCar = new StolenCar();
                $stolenCar->plate = $request->plate;
                $stolenCar->is_stolen = true;
                $stolenCar->description = $request->description;
                $stolenCar->save();
            }
            return response()->json([]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function addWantedCar(Request $request)
    {

        try {
            if (!$this->isJobPolice($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
                'description' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $wantedCar = WantedCar::where('plate', $request->plate)->first();
            if ($wantedCar) {
                $wantedCar->is_wanted = true;
                $wantedCar->reason = $request->description;
                $wantedCar->police_id = $this->getLogedInUserIdentifier($request);
                $wantedCar->officer_name = $this->getLogedInUserFullName($request);
                $wantedCar->save();
            } else {
                $wantedCar = new WantedCar();
                $wantedCar->plate = $request->plate;
                $wantedCar->is_wanted = true;
                $wantedCar->reason = $request->description;
                $wantedCar->police_id = $this->getLogedInUserIdentifier($request);
                $wantedCar->officer_name = $this->getLogedInUserFullName($request);
                $wantedCar->save();
            }
            return response()->json([]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function removeCarAsStolen(Request $request)
    {
        try {
            if (!$this->isJobPolice($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $stolenCar = StolenCar::where('plate', $request->plate)->first();
            if ($stolenCar) {
                $stolenCar->is_stolen = false;
                $stolenCar->save();
            }
            return response()->json([]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function removeWantedCar(Request $request)
    {
        try {
            if (!$this->isJobPolice($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $wantedCar = WantedCar::where('plate', $request->plate)->first();
            if ($wantedCar) {
                $wantedCar->is_wanted = false;
                $wantedCar->save();
            }
            return response()->json([]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function confiscateCar(Request $request)
    {

        try {
            if (!$this->isJobPolice($request)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'plate' => 'required',
                'newOwner' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $car = OwnedCar::where('plate', $request->plate)->with('user')->first();
            if ($car) {
                $carConfiscateLog = new ConfiscateCarLog();
                $carConfiscateLog->confiscator = $this->getLogedInUserIdentifier($request);
                $carConfiscateLog->confiscatorName = $this->getLogedInUserFullName($request);
                $carConfiscateLog->carOwner = $car->user->identifier;
                $carConfiscateLog->carOwnerName = $car->user->firstname . ' ' . $car->user->lastname;
                $carConfiscateLog->carPlate = $car->plate;
                $carConfiscateLog->reason = null;
                $carConfiscateLog->save();

                $car->owner = $request->newOwner;
                $car->save();
            }
            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function sellCar(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'plate' => 'required',
                'newOwner' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $car = OwnedCar::where('plate', $request->plate)->with('user')->first();

            if (!$car) {
                return response()->json('Car not found', 404);
            }

            if ($this->getLogedInUserIdentifier($request) !== $car->owner) {
                return response()->json('You are not a car owner', 404);
            }

            $carSellLog = new CarSellLogs();
            $carSellLog->seller = $this->getLogedInUserIdentifier($request);
            $carSellLog->buyer = $request->newOwner;
            $carSellLog->plate = $car->plate;
            $carSellLog->save();

            $car->owner = $request->newOwner;
            $car->save();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }


    protected function isJobPoliceAndMechanic(Request $request)
    {
        $user = $this->getLogedInUserData($request);
        if (
            !$user->hasRole('admin') and
            (!in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS['police']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS['mechanic']))
        ) {

            return false;
        }

        return true;
    }

    protected function isJobPolice(Request $request)
    {
        $user = $this->getLogedInUserData($request);
        if (
            !$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS['police'])
        ) {

            return false;
        }

        return true;
    }

    protected function getLogedInUserData(Request $request)
    {
        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(['roles', 'userWebUser'])->first();
    }

    protected function getLogedInUserActiveCharacterData(Request $request)
    {
        return User::where('identifier', $this->getLogedInUserIdentifier($request))->first();
    }

    protected function getLogedInUserIdentifier(Request $request)
    {
        if(!env('HEX_IDENTIFIER')) {
            return $this->getLogedInUserData($request)->userWebUser->user_identifier;
        }
        return $this->getLogedInUserData($request)->userWebUser->steam_hex;
    }

    protected function getLogedInUserFullName(Request $request)
    {

        return $this->getLogedInUserActiveCharacterData($request)->firstname . ' ' . $this->getLogedInUserActiveCharacterData($request)->lastname;
    }
}
