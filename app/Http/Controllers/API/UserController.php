<?php

namespace App\Http\Controllers\API;

use App\Models\CarModel;
use App\Models\House;
use App\Http\Controllers\API\Helper\UserBaseController;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\WebUser;
use App\Models\Whitelist;
use Carbon\Carbon;
use Illuminate\Http\Response;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Validator;

class UserController extends UserBaseController
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserData(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'identifier' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }
            $withRelations = [
                'criminalRecords',
                'criminalRecords.policeOfficer',
                'criminalRecords.policeOfficer.user',
                'medicalHistory',
                'medicalHistory.emsWorker',
                'medicalHistory.emsWorker.user',
                'cars',
                'cars.insurance',
                'cars.stolenCar',
                'cars.wantedCar',
                'wantedCharacter',
                'wantedCharacter.officer',
                'licenses',
                'bills',
            ];

            if (filter_var(getenv('PROPERTIES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
                $withRelations[] = 'ownedProperties';
                $withRelations[] = 'ownedProperties.property';
            }

            $user = User::where('identifier', $request->identifier)->with($withRelations)->first();
            $accounts = json_decode($user->accounts);
            $user->bank =  number_format($accounts->bank, 0, '.', ' ');
            $user->money =  number_format($accounts->money, 0, '.', ' ');
            $user->blackMoney =  number_format($accounts->black_money, 0, '.', ' ');
            foreach ($user->cars as $car) {
                $vehicleData = json_decode($car->vehicle);
                $carModel = CarModel::where('code', $vehicleData->model)->first();
                if ($carModel) {
                    $car->name = $carModel->name;
                } else {
                    $car->name = '';
                }
            }

            $cleanIdentifier = substr($request->identifier, strpos($request->identifier, ":") + 1);
            $user->house = $this->getCharacterHouses($cleanIdentifier, $user->firstname . ' ' . $user->lastname);

            if (!env('HEX_IDENTIFIER')) {
                $identifierColumnName = 'user_identifier';
            } else {
                $identifierColumnName = 'steam_hex';
            }

            $webUserId = DB::table('user_web_user')->where(
                $identifierColumnName,
                'LIKE',
                '%' . $cleanIdentifier . '%'
            )->first()->web_user_id;

            $webUser = WebUser::where('id', $webUserId)
                ->with(
                    [
                        'roles',
                        'banReasons' => function ($query) {
                            $query->orderBy('created_at', 'desc')->get();
                        }
                    ]
                )->first();

            $whitelist = Whitelist::where('identifier', $webUser->userWebUser->steam_hex)->first();
            $isBanned = false;
            if ($whitelist->ban_until && new Carbon($whitelist->ban_until) > new Carbon()) {
                $isBanned = true;
            }
            $response = [
                'user' => $user,
                'webUser' => $webUser,
                'licenses' => $this->getLicenses($user->licenses),
                'billsTotal' => number_format((new Collection($user->bills))->sum('amount'), 0, '.', ' '),
                'criminalRecordsCount' => $this->getCriminalRecordsCount($user->criminalRecords),
                'medicalHistoryCount' => $this->getMedicalRecordsCount($user->medicalHistory),
                'isBanned' => $isBanned
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * Get company money if user is boss
     *
     * @param Request $request
     * @return response
     */
    public function getCompanyMoney(Request $request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'hex' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            return response()->json(['companyMoney' => $this->companyMoney($request->hex)]);
        } catch (\Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getJobLabel(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'job' => 'required',
                'jobGrade' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            return response()->json($this->jobInfo($request->job, $request->jobGrade));
        } catch (\Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    private function getCharacterHouses($cleanIdentifier, $fullName)
    {
        $houses = House::where('owner', 'like', '%' . $cleanIdentifier)->get();
        $charHouses = [];
        foreach ($houses as $house) {
            unset($house->entry);
            unset($house->garage);
            unset($house->housekeys);
            unset($house->interior);
            unset($house->inventory);
            unset($house->inventorylocation);
            unset($house->last_repayment);
            unset($house->mortgage_owed);
            unset($house->resalejob);
            unset($house->resalepercent);
            unset($house->shells);
            unset($house->shell);
            unset($house->wardrobe);
            unset($house->doors);
            unset($house->furniture);
            unset($house->owned);
            if ($house->ownername == $fullName) {
                $charHouses[] = $house;
            }
        }
        return $charHouses;
    }
}
