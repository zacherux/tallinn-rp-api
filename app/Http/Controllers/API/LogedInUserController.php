<?php

namespace App\Http\Controllers\API;

use App\Models\DatastoreData;
use App\Models\Donation;
use App\Models\Dpkeybind;
use App\Http\Controllers\API\Helper\UserBaseController;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\WebUser;
use App\Models\Whitelist;
use App\Models\WhitelistApplications;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Validator;

class LogedInUserController extends UserBaseController
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getUserData(Request $request)
    {
        try {
            $webUserId = $request->user()->token()->user_id;
            $withRelations = [
                'roles',
                'banReasons' => function ($query) {
                    $query->orderBy('created_at', 'desc')->get();
                },
                'userWebUser',
                'priority'

            ];

            $user = WebUser::where('id', $webUserId)->with($withRelations)->first();
            if (!$user) {
                return response()->json(['no user found'], 404);
            }

            $whitelist = Whitelist::where('identifier', $user->userWebUser->steam_hex)->first();
            $isBanned = false;
            if ($whitelist->ban_until && new Carbon($whitelist->ban_until) > new Carbon()) {
                $isBanned = true;
            }

            $response = [
                'tokenData' => $request->user()->token(),
                'isBanned' => $isBanned,
                'user' => $user
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * Get company money if user is boss
     *
     * @param Request $request
     * @return response
     */
    public function getCompanyMoney(Request $request)
    {
        try {
            return response()->json(['companyMoney' => $this->companyMoney($this->getFivemIdentifier($request))]);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * get Loged in user whitelist application
     *
     * @param Request $request
     * @return Response
     */
    public function getWhitelistApplication(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $whitelistApplication = WhitelistApplications::where('web_user_id', $webUserId)->with(['admin'])->first();
            return response()->json($whitelistApplication);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    public function updateWhitelistApplication(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'whitelistId' => 'required',
            'characterName' => 'required',
            'steamName' => 'required',
            'steamHex' => 'required',
            'questions' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        //check unique steam hex. steam hex in database has brefix but user does not have to add brefix
        $steamId = 'steam:' . strtolower($request->steamHex);
        if (Whitelist::where('identifier', $steamId)->orWhere('identifier', 'ban:' . $request->steamHex)->first()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => [
                    'steamHex' => ['Hex already exist. Use another one']
                ]
            ];
            return response()->json($response, 404);
        }

        try {
            $wlApplication = WhitelistApplications::where('id', $request->whitelistId)->first();
            $wlApplication->characterName = $request->characterName;
            $wlApplication->steamName = $request->steamName;
            $wlApplication->user_identifier = $steamId;
            $wlApplication->extra_questions = $request->questions;
            $wlApplication->isRejected = false;
            $wlApplication->save();

            return response()->json([], 200);
        } catch (Exception $exception) {
            return $this->return($exception);
        }
    }

    public function deleteKeybinds(Request $request)
    {

        try {
            $hex = $this->getFivemIdentifier($request);
            $keybinds = Dpkeybind::where('id', $hex)->first();
            if ($keybinds) {
                $keybinds->delete();
            }

            return response()->json();
        } catch (Exception $exception) {
            return $this->returnError($exception);
        }
    }

    public function removeClouths(Request $request)
    {

        try {
            $hex = $this->getFivemIdentifier($request);
            $propertyCloset = DatastoreData::where('owner', $hex)->where('name', 'property')->first();
            if ($propertyCloset->data) {
                $data = json_decode($propertyCloset->data);
                $data->dressing = [];
                $propertyCloset->data = json_encode($data);
                $propertyCloset->save();
            }

            return response()->json();
        } catch (Exception $exception) {
            return $this->returnError($exception);
        }
    }

    public function storeDonation(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $donation = new Donation();
            $donation->web_user_id = $webUserId;
            $donation->explanation = $request->explanation;
            $donation->donation_number = $request->donationNumber;
            $donation->gifts = json_encode($request->gifts);
            $donation->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function isDonationNumberExist(Request $request)
    {
        try {
            $donationNr = Donation::where('donation_number', $request->donationNumber)->first();

            return response()->json(
                [
                    'exist' => ($donationNr) ? true : false
                ]
            );
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function updateUserData(Request $request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'name' => 'string|nullable',
                'email' => 'email|nullable',
                'oldPassword' => 'string|required_with:newPassword|nullable',
                'newPassword' => 'string|min:6|nullable',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 422);
            }
            $webUserId = $request->user()->token()->user_id;
            $user = DB::table('web_users')->where('id', $webUserId)->first();
            if ($request->oldPassword && !Hash::check($request->oldPassword, $user->password)) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => [
                        'oldPassword' => ['Passwords mismatch']
                    ]
                ];
                return response()->json($response, 422);
            }

            $user = WebUser::where('id', $webUserId)->first();
            if ($request->name) {
                $user->name = $request->name;
            }
            if ($request->email) {
                $user->email = $request->email;
            }

            if ($request->newPassword) {
                $user->password = bcrypt($request->newPassword);
            }

            $user->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * @param Request $request
     * @return string
     */
    protected function getFivemIdentifier(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();

        return $user->fivemUser[0]->identifier;
    }
}
