<?php

namespace App\Http\Controllers\API;

use App\Models\Bill;
use App\Http\Controllers\API\Helper\UserBaseController;
use App\Models\WebUser;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Validator;

class BillsController extends UserBaseController
{

    const ALLOWED_JOBS_MECHANIC = ['deluxe ', 'offdeluxe '];
    const ALLOWED_JOBS_POLICE = ['police', 'offpolice', 'abipolitsei'];
    const ALLOWED_JOBS_AMBULANCE = ['ambulance', 'offambulance'];
    const FIELDS_VALIDATION = [
        'clientId' => 'required',
        'amount' => 'required',
        'label' => 'required',
    ];

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createMechanicBill(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod']) and (!in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS_MECHANIC) || !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS_POLICE))) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }
            $validator = Validator::make($request->all(), self::FIELDS_VALIDATION);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            if(!env('HEX_IDENTIFIER')) {
                $compiler = $user->user_web_user->user_identifier;
            } else {
                $compiler = $user->user_web_user->steam_hex;
            }

            $this->createBill(
                $request->clientId,
                $compiler,
                'deluxe',
                $request->label,
                $request->amount
            );

            return response()->json();
        } catch (\Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function createPoliceBill(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod']) and (!in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS_AMBULANCE) or $user->fivemUser[0]->job_grade < 90)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }
            $validator = Validator::make($request->all(), self::FIELDS_VALIDATION);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            if(!env('HEX_IDENTIFIER')) {
                $compiler = $user->userWebUser->user_identifier;
            } else {
                $compiler = $user->userWebUser->steam_hex;
            }
            $this->createBill(
                $request->clientId,
                $compiler,
                'police',
                $request->label,
                $request->amount
            );

            return response()->json();
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    public function createAmbulanceBill(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod']) and (!in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS_POLICE))) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }
            $validator = Validator::make($request->all(), self::FIELDS_VALIDATION);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            if(!env('HEX_IDENTIFIER')) {
                $compiler = $user->userWebUser->user_identifier;
            } else {
                $compiler = $user->userWebUser->steam_hex;
            }
            $this->createBill(
                $request->clientId,
                $compiler,
                'ambulance',
                $request->label,
                $request->amount
            );

            return response()->json();
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    protected function createBill(
        $clientId,
        $compiler,
        $company,
        $label,
        $amount
    ) {

        try {
            $billing = new Bill();
            $billing->identifier = $clientId;
            $billing->sender = $compiler;
            $billing->target_type = 'society';
            $billing->target = 'society_' . $company;
            $billing->label = $label;
            $billing->amount = $amount;
            $billing->save();

            return $this;
        } catch (Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(
            [
                'roles',
                'userWebUser',
                'fiveMUser',
                'fiveMUser.policeOfficer',
            ]
        )->first();
    }
}
