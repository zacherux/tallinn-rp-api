<?php

namespace App\Http\Controllers\API;

use App\Models\GangApplication;
use App\Http\Controllers\Controller;
use App\Http\Requests\GangApplication\Accept;
use App\Http\Requests\GangApplication\Reject;
use App\Http\Requests\GangApplication\Save;
use Exception;
use Illuminate\Http\Request;

class GangApplicationController extends Controller
{

    public function save(Save $request)
    {

        try {

            $gangApplication = new GangApplication();
            $gangApplication->user_identifier = $request->identifier;
            $gangApplication->web_user_id = $request->user()->token()->user_id;
            $gangApplication->gang_name = $request->gangName;
            $gangApplication->answers = json_encode($request->questions);
            $gangApplication->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getGangApplications(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod'])) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            return response()->json(
                [
                    'gangApplications' => GangApplication::where('is_accepted', false)->where('is_rejected', false)->get()
                ]
            );
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getGangApplicationsCounts(Request $request)
    {
        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod'])) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $gangApplications = GangApplication::all();

            return response()->json(
                [
                    'total' => $gangApplications->count(),
                    'waiting' => $gangApplications->where('is_accepted', false)->where('is_rejected', false)->count()
                ]
            );
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function accept(Accept $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod'])) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $gangApplication = GangApplication::where('id', $request->id)->first();
            $gangApplication->is_accepted = true;
            $gangApplication->is_rejected = false;
            $gangApplication->admin_web_user_id = $user->id;
            $gangApplication->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function reject(Reject $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if (!$user->hasAnyRole(['admin', 'mod'])) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $gangApplication = GangApplication::where('id', $request->id)->first();
            $gangApplication->is_rejected = true;
            $gangApplication->is_accepted = false;
            $gangApplication->admin_web_user_id = $user->id;
            $gangApplication->rejection_reason = $request->reason;
            $gangApplication->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }
}
