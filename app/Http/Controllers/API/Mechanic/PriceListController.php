<?php

namespace App\Http\Controllers\API\Mechanic;

use App\Models\MechanicWorksList;
use App\Models\WebUser;
use Illuminate\Http\Request;
use Exception;
use Validator;

class PriceListController extends BaseController
{

    public function add(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS) and $user->fivemUser[0]->job_grade < 90) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'label' => 'required|string',
                'price' => 'required|integer',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $work = new MechanicWorksList();
            $work->label = $request->label;
            $work->price = $request->price;
            $work->save();

            return response()->json();

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getWorkList(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            return response()->json(MechanicWorksList::all());

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function update(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS) and $user->fivemUser[0]->job_grade < 90) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'label' => 'required|string',
                'price' => 'required|integer',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $work = MechanicWorksList::where('id', $request->id)->first();
            $work->label = $request->label;
            $work->price = $request->price;
            $work->save();

            return response()->json();

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function delete(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS) and $user->fivemUser[0]->job_grade < 90) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $work = MechanicWorksList::where('id', $request->id)->first();
            $work->delete();

            return response()->json();

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function search(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'keyword' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            return response()->json(MechanicWorksList::where('label', 'LIKE', '%' . $request->keyword . '%')->get());

        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
