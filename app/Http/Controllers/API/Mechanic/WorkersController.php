<?php

namespace App\Http\Controllers\API\Mechanic;

use App\Models\User;
use App\Models\WebUser;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Exception;

class WorkersController extends BaseController
{

    public function getWorkersList(Request $request)
    {
        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if (!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            return response()->json(
                User::where('job', 'deluxe')->orWhere('job', 'offdeluxe')->with(
                    [
                        'webUser',
                        'mechanicReports' => function ($query) {
                            $query->where('created_at', '>', Carbon::now()->subWeek())->orderBy('created_at', 'desc')->get();
                        }
                    ]
                )->get(),
                200
            );
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
