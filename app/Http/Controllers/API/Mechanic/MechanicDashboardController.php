<?php

namespace App\Http\Controllers\API\Mechanic;

use App\Models\Bill;
use Exception;
use App\Http\Controllers\Controller;
use App\Models\MechanicReports;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class MechanicDashboardController extends Controller
{

    public function getTopWorkers()
    {

        try {
            $reports = MechanicReports::where('created_at',  '>', Carbon::now()->subWeek())->with('user')->get();
            $collection = [];
            foreach ($reports as $report) {
                if (!array_key_exists($report->user_identifier, $collection)) {
                    $collection[$report->user_identifier] = [
                        'name' => $report->user->firstname . ' ' . $report->user->lastname,
                        'amount' => $report->job_price
                    ];
                } else {
                    $collection[$report->user_identifier]['amount'] = $collection[$report->user_identifier]['amount'] + $report->job_price;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['workers' => $collection->sortByDesc('amount')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getTopDebtors()
    {

        try {
            $bills = Bill::where('target', 'society_deluxe')->get();
            $collection = [];
            foreach ($bills as $bill) {
                if (!array_key_exists($bill->identifier, $collection)) {
                    $user = User::where('identifier', $bill->identifier)->first();
                    $collection[$bill->identifier] = [
                        'name' => $user->firstname . ' ' . $user->lastname,
                        'sum' => $bill->amount
                    ];
                } else {
                    $collection[$bill->identifier]['sum'] = $collection[$bill->identifier]['sum'] + $bill->amount;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['deptors' => $collection->sortByDesc('sum')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
