<?php

namespace App\Http\Controllers\API\Mechanic;

use App\Http\Controllers\Controller;

class BaseController extends Controller
{

    const ALLOWED_JOBS = ['deluxe', 'offdeluxe'];
}
