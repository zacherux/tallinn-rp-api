<?php

namespace App\Http\Controllers\API\Mechanic;

use App\Models\Bill;
use App\Models\MechanicReports;
use App\Models\MechanicVehicleParts;
use App\Models\WebUser;
use Illuminate\Http\Request;
use Exception;
use Validator;

class ReportsController extends BaseController
{

    public function createReport(Request $request)
    {
        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if (!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'workerHex' => 'required',
                'vehiclePlate' => 'required',
                'price' => 'required',
                'workList' => 'required',
                'partsList' => 'required',
                'payerHex' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $report = new MechanicReports();
            $report->user_identifier = $request->workerHex;
            $report->job_price = $request->price;
            $report->car_plate = $request->vehiclePlate;
            $report->summary = ($request->summary) ? $request->summary : '';
            $report->work_list = json_encode($request->workList);
            $report->parts_list = json_encode($request->partsList);
            $report->save();

            $bill = new Bill();
            $bill->identifier = $request->payerHex;
            $bill->sender = $user->fivemUser[0]->identifier;
            $bill->target_type = 'society';
            $bill->target = 'society_deluxe';
            $bill->label = 'Deluxe Autoäri';
            $bill->amount = $request->price;
            $bill->save();

            return response()->json([], 200);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function searchParts(Request $request)
    {
        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if (!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'keyword' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $partsSearch = MechanicVehicleParts::where('label', 'LIKE', '%' . strtolower($request->keyword) . '%')->get();

            return response()->json($partsSearch);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function savePart(Request $request)
    {

        try {
            $webUserId = $request->user()->token()->user_id;
            $user = WebUser::where('id', $webUserId)->with(['roles', 'fiveMUser'])->first();
            if (!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'label' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $part = MechanicVehicleParts::where('label', $request->label)->get();

            if ($part->count() < 1) {
                $part = new MechanicVehicleParts();
                $part->label = ucfirst($request->label);
                $part->save();
            }

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
