<?php

namespace App\Http\Controllers\API;

use Exception;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\WhitelistApplications;
use App\Models\Whitelist;
use Illuminate\Support\Facades\DB;
use Validator;

class WhitelistController extends Controller
{

    /**
     * @param Request $request
     * @return JsonResponse
     */
    public function getCounts(Request $request)
    {

        $request->user()->authorizeRoles('admin', 'mod');

        try {
            $wlApplications = WhitelistApplications::all();
            $response = [
                'total' => $wlApplications->count(),
                'rejected' => $wlApplications->where('isRejected', true)->count(),
                'accepted' => $wlApplications->where('isAccepted', true)->count(),
                'waiting' => $wlApplications->where('isAccepted', false)->where('isRejected', false)->count()
            ];
            return response()->json($response);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * @param Request $request
     * @return void
     */
    public function getWaitingApplications(Request $request)
    {

        $request->user()->authorizeRoles('admin', 'mod');

        try {
            return response()->json([
                'applications' => WhitelistApplications::where('isAccepted', false)->where('isRejected', false)->where('is_chat', false)->with(['webUser', 'admin'])->get()
            ]);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * @param Request $request
     * @return void
     */
    public function getinChatApplications(Request $request)
    {

        $request->user()->authorizeRoles('admin', 'mod');

        try {
            return response()->json([
                'applications' => WhitelistApplications::where('isAccepted', false)->where('isRejected', false)->where('is_chat', true)->with(['webUser', 'admin'])->get()
            ]);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * @param Request $request
     * @return response
     */
    public function acceptWhitelistApplication(Request $request)
    {

        $request->user()->authorizeRoles('admin', 'mod');

        $validator = Validator::make($request->all(), [
            'id' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        try {
            $whitelistApplication = WhitelistApplications::where('id', $request->id)->first();
            $whitelistApplication->isAccepted = true;
            $whitelistApplication->isRejected = false;
            $whitelistApplication->is_chat = false;
            $whitelistApplication->admin_identifier = $request->user()->id;
            $whitelistApplication->save();
            if (Whitelist::where('identifier', $whitelistApplication->user_identifier)->count() == 0) {
                $whitelist = new Whitelist();
                $whitelist->identifier = $whitelistApplication->user_identifier;
                $whitelist->save();
            }
            if (DB::table('user_web_user')->where('steam_hex', '=', $whitelistApplication->user_identifier)->count() === 0) {
                DB::table('user_web_user')->insert([
                    'web_user_id' => $whitelistApplication->web_user_id,
                    'steam_hex' => $whitelistApplication->user_identifier
                ]);
            }
            return response()->json([], 200);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * @param Request $request
     * @return response
     */
    public function rejectWhitelistApplication(Request $request)
    {

        $request->user()->authorizeRoles('admin', 'mod');

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'rejectionReason' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        try {
            $whitelistApplication = WhitelistApplications::where('id', $request->id)->first();
            $whitelistApplication->isAccepted = false;
            $whitelistApplication->isRejected = true;
            $whitelistApplication->is_chat = false;
            $whitelistApplication->rejectionReason = $request->rejectionReason;
            $whitelistApplication->admin_identifier = $request->user()->id;
            $whitelistApplication->save();
            return response()->json([], 200);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * @param Request $request
     * @return response
     */
    public function inviteToChat(Request $request)
    {

        $request->user()->authorizeRoles('admin', 'mod');

        $validator = Validator::make($request->all(), [
            'id' => 'required',
            'rejectionReason' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        try {
            $whitelistApplication = WhitelistApplications::where('id', $request->id)->first();
            $whitelistApplication->isAccepted = false;
            $whitelistApplication->isRejected = false;
            $whitelistApplication->is_chat = true;
            $whitelistApplication->rejectionReason = $request->rejectionReason;
            $whitelistApplication->admin_identifier = $request->user()->id;
            $whitelistApplication->save();
            return response()->json([], 200);
        } catch (\Exception $e) {
            return $this->returnError($e);
        }
    }

    /**
     * Create whitelist application for existing user
     *
     * @param Request $request
     * @return void
     */
    public function createWhitelistApplicationForExistingUser(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'characterName' => 'required',
            'steamName' => 'required',
            'steamHex' => 'required',
            'questions' => 'required'
        ]);

        if ($validator->fails()) {
            $response = [
                'success' => false,
                'data' => 'Validation Error.',
                'messages' => $validator->errors()
            ];
            return response()->json($response, 404);
        }

        //chekc unique steam hex. steam hex in database has brefix but user does not have to add brefix
        $steamId = 'steam:' . strtolower($request->steamHex);
        $alreadyWhitelisted = false;
        if (Whitelist::where('identifier', $steamId)->orWhere('identifier', 'ban:' . $request->steamHex)->first()) {
            $alreadyWhitelisted = true;
        } else {
            DB::table('user_web_user')->insert([
                'web_user_id' => $request->user()->token()->user_id,
                'user_identifier' => $steamId
            ]);
        }

        try {

            // create whitelist application
            $wlApp = new WhitelistApplications();
            $wlApp->user_identifier = $steamId;
            $wlApp->steamName = $request->steamName;
            $wlApp->characterName = $request->characterName;

            $wlApp->isAccepted = ($alreadyWhitelisted) ? 1 : 0;
            $wlApp->isRejected = 0;
            $wlApp->is_chat = false;
            $wlApp->rejectionReason = null;
            $wlApp->extra_questions = json_encode($request->questions);
            $wlApp->web_user_id = $request->user()->token()->user_id;
            $wlApp->save();

            return response()->json(
                [
                    'isSuccess' => true,
                    'alreadyWhitelisted' => true,
                ],
                200
            );
        } catch (Exception $exception) {
            return $this->returnError($exception);
        }
    }
}
