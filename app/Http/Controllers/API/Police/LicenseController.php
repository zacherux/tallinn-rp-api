<?php

namespace App\Http\Controllers\API\Police;

use App\Http\Controllers\Controller;
use App\Models\UserLicenses;
use App\Models\WebUser;
use Illuminate\Http\Request;
use Exception;
use Validator;

class LicenseController extends Controller
{

    const ALLOWED_JOBS = ['police', 'offpolice', 'abipolitsei'];

    public function remove(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'clientId' => 'required',
                'licenseType' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $licenseType = $this->getLicenseType($request->licenseType);

            if (!$licenseType) {
                $response = [
                    'success' => false,
                    'data' => 'Licens type not found.',
                ];
                return response()->json($response, 421);
            }

            UserLicenses::where('owner', $request->clientId)
                ->where('type', $licenseType)
                ->delete();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function give(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasAnyRole(['admin', 'mod']) and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'clientId' => 'required',
                'licenseType' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $licenseType = $this->getLicenseType($request->licenseType);

            if (!$licenseType) {
                $response = [
                    'success' => false,
                    'data' => 'Licens type not found.',
                ];
                return response()->json($response, 421);
            }

            $license = new UserLicenses();
            $license->type = $licenseType;
            $license->owner = $request->clientId;
            $license->save();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    protected function getLicenseType($requestLicenseType)
    {

        $licenseType = null;
        switch ($requestLicenseType) {
            case 'theory':
                $licenseType = 'dmv';
                break;
            case 'aCategory':
                $licenseType = 'drive_bike';
                break;
            case 'bCategory':
                $licenseType = 'drive';
                break;
            case 'cCategory':
                $licenseType = 'drive_truck';
                break;
            case 'dCategory':
                $licenseType = 'drive_bus';
                break;
            case 'weapon':
                $licenseType = 'weapon';
                break;
            case 'hunting':
                $licenseType = 'hunting';
                break;
            case 'fishing':
                $licenseType = 'fishing';
                break;
            case 'boat':
                $licenseType = 'boat';
                break;

            default:
                $licenseType = null;
                break;
        }

        return $licenseType;
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(
            [
                'roles',
                'fiveMUser',
                'fiveMUser.policeOfficer',
            ]
        )->first();
    }
}
