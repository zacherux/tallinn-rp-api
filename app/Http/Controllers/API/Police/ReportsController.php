<?php

namespace App\Http\Controllers\API\Police;

use App\Models\Bill;
use App\Models\CriminalRecord;
use App\Models\FineType;
use App\Http\Controllers\Controller;
use App\Models\PoliceOfficer;
use App\Models\User;
use App\Models\WantedCharacters;
use App\Models\WebUser;
use Illuminate\Http\Request;
use Exception;
use Validator;

class ReportsController extends Controller
{

    const ALLOWED_JOBS = ['police', 'offpolice', 'abipolitsei'];

    public function getUserFines(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'criminalId' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }
            $crimibnalUser = User::where('identifier', $request->criminalId)->with('bills')->first();
            $bills = $crimibnalUser->bills()->where('target', 'society_police')->get();
            $fines = [];
            foreach($bills as $bill) {
                $label = substr(strstr($bill->label, ' '), 1);
                $fine = FineType::where('label', $label)->first();
                if ($fine){
                    $fines[] = $fine;
                }
            }

            return response()->json(['fines' => $fines]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function searchFines(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'keyword' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $fines = FineType::where('label', 'LIKE', '%' . $request->keyword . '%')->get();
            return response()->json(['fines' => $fines]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function addFineBill(Request $request)
    {

        try {
            $user = $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'fineId' => 'required',
                'criminalHex' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $fine = FineType::where('id', $request->fineId)->first();
            $bill = new Bill();
            $bill->identifier = $request->criminalHex;
            $bill->sender = $user->fivemUser[0]->identifier;
            $bill->target_type = 'society';
            $bill->target = 'society_police';
            $bill->label = 'Trahv: ' . $fine->label;
            $bill->amount = $fine->amount;
            $bill->save();

            return response()->json(['fine' => 'added']);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function saveReport(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'criminalHex' => 'required',
                'summary' => 'required',
                'isCriminalCase' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            if (!$user->fivemUser[0]->policeOfficer) {
                $policeOfficer = new PoliceOfficer();
                $policeOfficer->user_identifier = $user->fivemUser[0]->identifier;
                $policeOfficer->is_hired = true;
                $policeOfficer->save();

                $user = $this->getLogedInUserData($request);
            }

            $criminalData = User::where('identifier', $request->criminalHex)->first();
            $criminalReport = new CriminalRecord();
            $criminalReport->user_identifier = $request->criminalHex;
            $criminalReport->character_name = $criminalData->firstname . ' ' . $criminalData->lastname;
            $criminalReport->offence_type = 'puudub';
            $criminalReport->fine = ($request->fines) ? json_encode($request->fines) : '{}';
            $criminalReport->jail_time = ($request->jailTime) ? $request->jailTime : 0;
            $criminalReport->warning_car = ($request->warning) ? true : false;
            $criminalReport->warning_driver_license = ($request->warning) ? true : false;
            $criminalReport->warning_gun_license = ($request->warning) ? true : false;
            $criminalReport->summary = $request->summary;
            $criminalReport->is_criminal_case = ($request->isCriminalCase) ? true : false;
            $criminalReport->criminal_case_expires_at = ($request->isCriminalCase) ? (new \DateTime())->modify('+1 week') : null;
            $criminalReport->police_officer_id = $user->fivemUser[0]->policeOfficer->id;
            $criminalReport->place = ($request->loacation) ? $request->loacation : '';
            $criminalReport->car_plate = ($request->vehiclePlate) ? $request->vehiclePlate : '';
            $criminalReport->other_parties = ($request->otherParties) ? $request->otherParties : '';
            $criminalReport->save();

            return response()->json(['report' => 'added']);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function deleteRaport(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'id' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $raport = CriminalRecord::where('id', $request->id)->delete();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function declareUserAsWanted(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'criminalId' => 'required',
                'summary' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $criminal = User::where('identifier', $request->criminalId)->first();
            $wanted = WantedCharacters::where('user_identifier', $request->criminalId)->first();
            if(!$wanted) {
                $wanted = new WantedCharacters();
                $wanted->user_identifier = $request->criminalId;
                $wanted->character_name = $criminal->firstname . ' ' . $criminal->lastname;
                $wanted->is_wanted = true;
                $wanted->reason = $request->summary;
                $wanted->creator_id = $user->fivemUser[0]->identifier;
                $wanted->creator_name = $user->fivemUser[0]->firstname . ' ' . $user->fivemUser[0]->lastname;
                $wanted->save();
            } else {
                $wanted->character_name = $criminal->firstname . ' ' . $criminal->lastname;
                $wanted->is_wanted = true;
                $wanted->reason = $request->summary;
                $wanted->creator_id = $user->fivemUser[0]->identifier;
                $wanted->creator_name = $user->fivemUser[0]->firstname . ' ' . $user->fivemUser[0]->lastname;
                $wanted->save();
            }

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function removeWantedFromperson(Request $request)
    {

        try {
            $user = $this->getLogedInUserData($request);
            if(!$user->hasRole('admin') and !in_array($user->fivemUser[0]->job, self::ALLOWED_JOBS)) {
                $response = [
                    'messages' => 'You have unauthorized job or role'
                ];
                return response()->json($response, 401);
            }

            $validator = Validator::make($request->all(), [
                'wantedId' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $wanted = WantedCharacters::where('id', $request->wantedId)->delete();

            return response()->json();
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    protected function getLogedInUserData(Request $request)
    {

        $webUserId = $request->user()->token()->user_id;
        return WebUser::where('id', $webUserId)->with(
            [
                'roles',
                'fiveMUser',
                'fiveMUser.policeOfficer',
            ]
        )->first();
    }
}
