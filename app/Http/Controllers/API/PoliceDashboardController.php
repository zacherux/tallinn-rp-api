<?php

namespace App\Http\Controllers\API;

use App\Models\Bill;
use App\Models\CriminalRecord;
use Exception;
use Validator;
use App\Http\Controllers\Controller;
use App\Models\JobApplication;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class PoliceDashboardController extends Controller
{

    public function getLatestCiminalRecords()
    {
        try{
            $ciminalRecords = CriminalRecord::orderBy('id', 'desc')->limit(5)->get();
            return response()->json(['records' => $ciminalRecords]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getTopCriminalsOfWeek()
    {

        try{
            $ciminalRecords = CriminalRecord::where('created_at', '>', Carbon::now()->subWeek())->orderBy('id', 'desc')->get();
            $collection = [];
            foreach($ciminalRecords as $record) {
                if (!array_key_exists($record->user_identifier, $collection)) {
                    $collection[$record->user_identifier] = [
                        'name' => $record->character_name,
                        'count' => 1
                    ];
                } else {
                    $collection[$record->user_identifier]['count']++;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['records' => $collection->sortByDesc('count')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getTopDebtors()
    {

        try{
            $bills = Bill::where('target', 'society_police')->get();
            $collection = [];
            foreach($bills as $bill) {
                if(!array_key_exists($bill->identifier, $collection)) {
                    $user = User::where('identifier', $bill->identifier)->first();
                    $collection[$bill->identifier] = [
                        'name' => $user->firstname . ' ' . $user->lastname,
                        'sum' => $bill->amount
                    ];
                } else {
                    $collection[$bill->identifier]['sum'] = $collection[$bill->identifier]['sum'] + $bill->amount;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['deptors' => $collection->sortByDesc('sum')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getTopPolices()
    {

        try{
            $records = CriminalRecord::where('created_at',  '>', Carbon::now()->subWeek())->with(['policeOfficer.user'])->orderBy('id', 'DESC')->get();;
            $collection = [];
            foreach($records as $record) {
                if(!array_key_exists($record->policeOfficer->user_identifier, $collection)) {
                    $collection[$record->policeOfficer->user_identifier] = [
                        'name' => $record->policeOfficer->user->firstname . ' ' . $record->policeOfficer->user->lastname,
                        'count' => 1
                    ];
                } else {
                    $collection[$record->policeOfficer->user_identifier]['count']++;
                }
            }
            $collection = new Collection($collection);
            return response()->json(['officers' => $collection->sortByDesc('count')->take(5)]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function getJobAppliactionsCounts()
    {

        try{
            $jobApplications = JobApplication::where('job', 'police')->get();
            return response()->json([
                'total' => $jobApplications->count(),
                'unchecked' => $jobApplications->where('isAccepted', false)->where('isRejected', false)->where('initiated', false)->count(),
                'waiting' => $jobApplications->where('isAccepted', true)->where('isRejected', false)->where('initiated', false)->count(),
            ]);
        } catch (Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
