<?php

namespace App\Http\Controllers\API;

use Exception;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\OwnedCar;
use App\Models\User;
use App\Models\WebUser;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{

    public function getSearchResults(Request $request)
    {

        try {
            $keyword = strtolower($request->search);
            $keyword = '%' . $keyword . '%';
            $users = User::whereRaw('LOWER(`firstname`) like ?', $keyword)
                ->orWhereRaw('LOWER(`lastname`) like ?', $keyword)
                ->orWhereRaw('`phone_number` like ?', $keyword)
                ->get();

            $cars = OwnedCar::whereRaw('LOWER(`plate`) like ?', $keyword)->get();

            if (filter_var(getenv('PROPERTIES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
                $properties = DB::table('properties')
                    ->whereRaw('LOWER(`name`) like ?', $keyword)
                    ->orWhereRaw('LOWER(`label`) like ?', $keyword)
                    ->get();
            }
            $results = [
                'search' => $request->search,
                'persons' => $users,
                'vehicles' => $cars
            ];

            if (filter_var(getenv('PROPERTIES_EXIST'), FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE)) {
                $results['properties'] = $properties;
            }

            return response()->json(['results' => $results]);
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function searchWebUser(Request $request)
    {
        try {
            $keyword = strtolower($request->search);
            $keyword = '%' . $keyword . '%';

            $webUser = WebUser::whereRaw('LOWER(`email`) like ?', $keyword)
                ->orWhereRaw('LOWER(`name`) like ?', $keyword)
                ->with('userWebUser')
                ->get();

            return response()->json(['results' => $webUser]);
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }
}
