<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\WebUser;
use App\Models\Whitelist;
use App\Models\WhitelistBanReason;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Validator;

class PenaltyController extends Controller
{

    public function addPunishment(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'webUserId' => 'required|int',
                'reason'  => 'required|string',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 420);
            }

            $punisment = new WhitelistBanReason();
            $punisment->web_user_id = $request->webUserId;
            $punisment->reason = $request->reason;
            $punisment->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function addWhitelistBan(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'webUserId' => 'required|int',
                'reason'  => 'required|string',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 420);
            }

            $punisment = new WhitelistBanReason();
            $punisment->web_user_id = $request->webUserId;
            $punisment->reason = $request->reason;
            $punisment->save();

            $this->updateWhitelist($request->webUserId, $request->reason, (new Carbon())->addYears(10));

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function removeWhitelistBan(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'webUserId' => 'required|int',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 420);
            }

            $this->updateWhitelist($request->webUserId, null, null);

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    private function updateWhitelist($webUserId, $reason, $date)
    {

        $webUser = WebUser::where('id', $webUserId)->with(['userWebUser'])->first();
        $whitelist = Whitelist::where('identifier', $webUser->userWebUser->steam_hex)->first();
        $whitelist->ban_reason = $reason;
        $whitelist->ban_until = $date;
        $whitelist->save();

        return;
    }
}
