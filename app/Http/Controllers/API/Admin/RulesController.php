<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\Rules;
use App\Models\RulesCategory;
use Illuminate\Http\Request;
use Exception;
use Validator;

class RulesController extends Controller
{

    public function addRule(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'rule' => 'required',
                'category' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $category = RulesCategory::where('name', $request->category)->first();
            if(!$category) {
                $category = new RulesCategory();
                $category->name = $request->category;
                $category->save();
            }

            $rule = new Rules();
            $rule->rules_category_id = $category->id;
            $rule->rule = $request->rule;
            $rule->save();

            return response()->json();
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function update(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'id'  => 'required',
                'rule' => 'required',
                'category' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $category = RulesCategory::where('name', $request->category)->first();
            if(!$category) {
                $category = new RulesCategory();
                $category->name = $request->category;
                $category->save();
            }

            $oldCategory = null;
            $rule = Rules::where('id', $request->id)->with('category', 'category.rules')->first();
            if($rule->category->rules->count() < 2 ) {
                $oldCategory = RulesCategory::where('id', $rule->category->id)->with('rules')->first();
            }
            $rule->rules_category_id = $category->id;
            $rule->rule = $request->rule;
            $rule->save();

            if ($oldCategory) {
                $oldCategory->refresh();
                if ($oldCategory->rules->count() < 1) {
                    $oldCategory->delete();
                }
            }

            return response()->json();
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function delete(Request $request)
    {
        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'id'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $rule = Rules::where('id', $request->id)->first();
            $categoryId = $rule->rules_category_id;
            $category = RulesCategory::where('id', $categoryId)->with('rules')->first();
            $rule->delete();
            if ($category->refresh()->rules->count() < 1) {
                $category->delete();
            }

            return response()->json();
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function searchCategory(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'keyword'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            return response()->json(
                [
                    'results' => RulesCategory::where('name', 'LIKE', '%'. $request->keyword . '%')->get()
                ]
            );
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
