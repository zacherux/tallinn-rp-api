<?php

namespace App\Http\Controllers\API\Admin;

use App\Models\Donation;
use App\Models\DonationGoal;
use App\Http\Controllers\Controller;
use DateTime;
use Illuminate\Http\Request;
use Exception;
use Validator;

class DonationController extends Controller
{

    public function getPendingDonations(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');
            $donations = Donation::where('is_approved', false)->with(['webUser'])->get();

            return response()->json([
                'pending' => $donations
            ]);
        }catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getWaitingGiftDonations(Request $request)
    {
        try {
            $request->user()->authorizeRoles('admin', 'mod');
            $donations = Donation::where('is_approved', true)->where('is_gift_given', false)->with(['webUser'])->get();
            return response()->json([
                'waiting' => $donations
            ]);
        }catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function deleteDonation(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');
            $validator = Validator::make($request->all(), [
                'id'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $donation = Donation::where('id', $request->id)->first();
            $donation->delete();

            return response()->json();
        }catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function confirmDonation(Request $request) {
        try{
            $request->user()->authorizeRoles('admin');
            $validator = Validator::make($request->all(), [
                'id'  => 'required',
                'amount'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $donation = Donation::where('id', $request->id)->first();
            $donation->amount = $request->amount;
            $donation->is_approved = true;
            $donation->save();
            return response()->json();
        }catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function confirmGift(Request $request)
    {

        try{

            $request->user()->authorizeRoles('admin', 'mod');
            $validator = Validator::make($request->all(), [
                'id'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $donation = Donation::where('id', $request->id)->first();
            $donation->is_gift_given = true;
            $donation->save();
            return response()->json();
        } catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getConfirmedDonations(Request $request)
    {

        try{

            $request->user()->authorizeRoles('admin', 'mod');

            $now = new DateTime();
            $donations = Donation::where('is_approved', true)->where('is_gift_given', true)->whereMonth('updated_at', $now->format('m'))->with(['webUser'])->get();
            return response()->json([
                'confirmed' => $donations
            ]);
            return response()->json();
        } catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getThisMonthDonationsSum(Request $request)
    {

        try{

            $now = new DateTime();
            $sum = Donation::where('is_approved', true)->whereMonth('updated_at', $now->format('m'))->sum('amount');
            $goal = DonationGoal::first();

            return response()->json([
                'sum' => $sum,
                'goal' => ($goal) ? $goal->amount : 0,
            ]);
            return response()->json();
        } catch(Exception $e) {
            return $this->returnError($e);
        }
    }

    public function updateGoal(Request $request)
    {

        try{
            $request->user()->authorizeRoles('admin');
            $validator = Validator::make($request->all(), [
                'amount'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $donationGoal = DonationGoal::first();
            if(!$donationGoal) {
                $donationGoal = new DonationGoal();
            }
            $donationGoal->amount = $request->amount;
            $donationGoal->save();
            return response()->json();
        }catch(Exception $e) {
            return $this->returnError($e);
        }
    }
}
