<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\Info;
use App\Models\InfoCategory;
use Illuminate\Http\Request;
use Exception;
use Illuminate\Support\Facades\Log;
use Validator;

class InfoController extends Controller
{

    public function addInfo(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'info' => 'required',
                'category' => 'required'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $category = InfoCategory::where('name', $request->category)->first();
            if(!$category) {
                $category = new InfoCategory();
                $category->name = $request->category;
                $category->save();
            }

            $info = new Info();
            $info->info_categories_id = $category->id;
            $info->info = $request->info;
            $info->save();

            return response()->json();
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function update(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'id'  => 'required',
                'info' => 'required',
                'category' => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $category = InfoCategory::where('name', $request->category)->first();
            if(!$category) {
                $category = new InfoCategory();
                $category->name = $request->category;
                $category->save();
            }

            $oldCategory = null;
            $info = Info::where('id', $request->id)->with('category', 'category.info')->first();
            if($info->category->info->count() < 2 ) {
                $oldCategory = InfoCategory::where('id', $info->category->id)->with('info')->first();
            }
            $info->info_categories_id = $category->id;
            //$info->info = $request->info;
            $info->save();

            if ($oldCategory) {
                $oldCategory->refresh();
                if($oldCategory->info->count() == 0) {
                    $oldCategory->delete();
                }
            }

            return response()->json();
        }catch(Exception $e) {
            Log::error($e->getMessage());
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function delete(Request $request)
    {
        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'id'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            $info = Info::where('id', $request->id)->first();
            $categoryId = $info->info_categories_id;
            $category = InfoCategory::where('id', $categoryId)->with('info')->first();
            $info->delete();
            if ($category->refresh()->info->count() < 1) {
                $category->delete();
            }

            return response()->json();
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }

    public function searchCategory(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'keyword'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 404);
            }

            return response()->json(
                [
                    'results' => InfoCategory::where('name', 'LIKE', '%'. $request->keyword . '%')->get()
                ]
            );
        }catch(Exception $e) {
            return response()->json(
                [
                    'error' => 'There was some uexpected error',
                    'fulleErrorMessage' => $e->getMessage()
                ],
                (is_numeric($e->getCode()) and $e->getCode() !== 0) ? $e->getCode() : 500
            );
        }
    }
}
