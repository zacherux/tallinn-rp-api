<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\Controller;
use App\Models\PriorityQueue;
use App\Models\Whitelist;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Exception;
use Validator;

class PriorityController extends Controller
{

    public function getPriorityList(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $prioList = PriorityQueue::with('webUser.userWebUser')->get();

            $completeList = [
                'expired' => [],
                'expiring' => [],
                'existing' => []
            ];

            $today = Carbon::now();
            $fiveDaysAgo = (Carbon::now()->addDays(5));
            foreach ($prioList as $prio) {
                $endDate = new Carbon($prio->end_date);
                if ($endDate->lessThan($today)) {
                    $completeList['expired'][] = $prio;
                } elseif ($endDate->lessThan($fiveDaysAgo)) {
                    $completeList['expiring'][] = $prio;
                } else {
                    $completeList['existing'][] = $prio;
                }
            }

            return response()->json($completeList);
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function add(Request $request)
    {

        try {

            $validator = Validator::make($request->all(), [
                'webUserId' => 'required|integer',
                'hex'  => 'required|string',
                'endDate'  => 'required|string',
                'power'  => 'required|string',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $prio = new PriorityQueue();
            $prio->web_user_id = $request->webUserId;
            $prio->end_date = new Carbon($request->endDate);
            $prio->power = $request->power;
            $prio->save();

            $whitelist = Whitelist::where('identifier', $request->hex)->first();
            $whitelist->vip = $request->power;
            $whitelist->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function getCounts(Request $request)
    {

        try {

            $request->user()->authorizeRoles('admin', 'mod');

            $prioList = PriorityQueue::all();

            $expired = [];

            $today = Carbon::now();
            foreach ($prioList as $prio) {
                $endDate = new Carbon($prio->end_date);
                if ($endDate->lessThan($today)) {
                    $expired[] = $prio;
                }
            }

            return response()->json([
                'expired' => count($expired),
                'total' => $prioList->count()
            ]);
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function delete(Request $request)
    {

        try {
            $request->user()->authorizeRoles('admin', 'mod');
            $validator = Validator::make($request->all(), [
                'id' => 'required|integer'
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $prio = PriorityQueue::where('id', $request->id)->with('webUser.userWebUser')->first();
            $whitelist = Whitelist::where('identifier', $prio->webUser->userWebUser->steam_hex)->first();
            $whitelist->vip = 0;
            $whitelist->save();
            $prio->delete();
            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }

    public function update(Request $request)
    {
        try {
            $request->user()->authorizeRoles('admin', 'mod');

            $validator = Validator::make($request->all(), [
                'id' => 'required',
                'hex'  => 'required|string',
                'endDate'  => 'required|string',
                'power'  => 'required',
            ]);

            if ($validator->fails()) {
                $response = [
                    'success' => false,
                    'data' => 'Validation Error.',
                    'messages' => $validator->errors()
                ];
                return response()->json($response, 421);
            }

            $prio = PriorityQueue::find($request->id);
            $prio->end_date = new Carbon($request->endDate);
            $prio->power = $request->power;
            $prio->save();

            $whitelist = Whitelist::where('identifier', $request->hex)->first();
            $whitelist->vip = $request->power;
            $whitelist->save();

            return response()->json();
        } catch (Exception $e) {
            return $this->returnError($e);
        }
    }
}
