<?php

namespace App\Http\Controllers\API\Helper;

use App\Models\AddonAccountData;
use App\Http\Controllers\Controller;
use App\Models\Job;
use App\Models\JobGrades;
use App\Models\User;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Collection;

class UserBaseController extends Controller
{

    /**
     * Get user licenses
     *
     * @param Collection $userLicenses
     * @return array
     */
    protected function getLicenses(Collection $userLicenses): array
    {
        return [
            'theory' => $userLicenses->where('type', 'dmv')->count() > 0,
            'aCategory' => $userLicenses->where('type', 'drive_bike')->count() > 0,
            'bCategory' => $userLicenses->where('type', 'drive')->count() > 0,
            'cCategory' => $userLicenses->where('type', 'drive_truck')->count() > 0,
            'dCategory' => $userLicenses->where('type', 'drive_bus')->count() > 0,
            'boat' => $userLicenses->where('type', 'boat')->count() > 0,
            'fishing' => $userLicenses->where('type', 'fishing')->count() > 0,
            'hunting' => $userLicenses->where('type', 'hunting')->count() > 0,
            'weapon' => $userLicenses->where('type', 'weapon')->count() > 0
        ];
    }

    /**
     * Get user criminal records counts
     *
     * @param Collection $criminalRecords
     * @return Collection
     */
    protected function getCriminalRecordsCount(Collection $criminalRecords): Collection
    {
        return new Collection([
            'total' => $criminalRecords->count(),
            'criminalCases' => $criminalRecords->where('is_criminal_case', true)->count(),
            'activeCriminalCases' => $criminalRecords->where('is_criminal_case', true)->where('criminal_case_expires_at', '>', Carbon::now())->count(),
        ]);
    }

    /**
     * Get user medical records counts
     *
     * @param Collection $medicalRecords
     * @return Collection
     */
    protected function getMedicalRecordsCount(Collection $medicalRecords): Collection
    {
        return new Collection([
            'total' => $medicalRecords->count(),
            'week' => $medicalRecords->where('created_at', '>', Carbon::now()->subWeek())->count()
        ]);
    }

    /**
     * Get company money if user is boss
     *
     * @param string $hex
     * @return int
     */
    protected function companyMoney(string $hex): int
    {
        try {
            $user = User::where('identifier', $hex)->first();
            if ($user->job_grade < 90) {
                return 0;
            }
            $accountData = AddonAccountData::where('account_name', 'society_' . $user->job)->first();
            if (!$accountData) {
                return 0;
            }
            return number_format($accountData->money, 0, '.', ' ');
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }

    /**
     * Get job info
     *
     * @param string $job
     * @param int $jobGrade
     * @return array
     * @throws Exception
     */
    protected function jobInfo(string $job, int $jobGrade): array
    {

        try {
            $jobGradeFullInfo = JobGrades::where('job_name', $job)->where('grade', $jobGrade)->first();
            $jobFullInfo = Job::where('name', $job)->first();
            return [
                'jobLabel' => $jobFullInfo->label,
                'jobGradeLabel' => $jobGradeFullInfo->label,
                'salary' => $jobGradeFullInfo->salary
            ];
        } catch (\Exception $e) {
            throw new Exception($e->getMessage(), $e->getCode());
        }
    }
}
