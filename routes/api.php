<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('api')->post('register', 'API\AuthController@register');
Route::middleware('api')->post('login', 'API\AuthController@login');
Route::middleware('api')->post('registerWhitelist', 'API\AuthController@createWhitelistApplication');
Route::middleware('api')->get('getRules', 'API\RulesController@getAllRules');
Route::middleware('api')->get('getRuleCategories', 'API\RulesController@getRuleCategories');
Route::middleware('api')->get('getAllInfo', 'API\RulesController@getAllInfo');
Route::middleware('api')->get('getInfoCategories', 'API\RulesController@getInfoCategories');
Route::middleware('api')->get('health', function () {
    return response()->json(['health' => 'OK']);
});

// Authenticated user self info paths
Route::middleware('auth:api')->get('/auth/user/getUserData', 'API\LogedInUserController@getUserData');
Route::middleware('auth:api')->get('/auth/user/getCharacters', 'API\LogedInUserCharactersController@getCharacters');
Route::middleware('auth:api')->get('/auth/user/getCompanyMoney', 'API\LogedInUserController@getCompanyMoney');
Route::middleware('auth:api')->get('/auth/user/getWhitelistApplication', 'API\LogedInUserController@getWhitelistApplication');
Route::middleware('auth:api')->put('/auth/user/updateWhitelistApplication', 'API\LogedInUserController@updateWhitelistApplication');
Route::middleware('auth:api')->post('/auth/user/createWhitelistApplicationForExistingUser', 'API\WhitelistController@createWhitelistApplicationForExistingUser');
Route::middleware('auth:api')->delete('/auth/user/deleteKeybinds', 'API\LogedInUserController@deleteKeybinds');
Route::middleware('auth:api')->delete('/auth/user/removeClouths', 'API\LogedInUserController@removeClouths');
Route::middleware('auth:api')->post('/auth/user/donation', 'API\LogedInUserController@storeDonation');
Route::middleware('auth:api')->get('/auth/user/isDonationNumberExist', 'API\LogedInUserController@isDonationNumberExist');
Route::middleware('auth:api')->post('/auth/user/updateData', 'API\LogedInUserController@updateUserData');

//Police routes
Route::middleware('auth:api')->get('/police/getLatestCiminalRecords', 'API\PoliceDashboardController@getLatestCiminalRecords');
Route::middleware('auth:api')->get('/police/getTopCriminalsOfWeek', 'API\PoliceDashboardController@getTopCriminalsOfWeek');
Route::middleware('auth:api')->get('/police/getTopDebtors', 'API\PoliceDashboardController@getTopDebtors');
Route::middleware('auth:api')->get('/police/getTopPolices', 'API\PoliceDashboardController@getTopPolices');
Route::middleware('auth:api')->get('/police/getJobAppliactionsCounts', 'API\PoliceDashboardController@getJobAppliactionsCounts');

//Ambulance routes
Route::middleware('auth:api')->get('/ambulance/getTopDebtors', 'API\AmbulanceDashboardController@getTopDebtors');
Route::middleware('auth:api')->get('/ambulance/getTopWorkers', 'API\AmbulanceDashboardController@getTopWorkers');
Route::middleware('auth:api')->get('/ambulance/getJobAppliactionsCounts', 'API\AmbulanceDashboardController@getJobAppliactionsCounts');
Route::middleware('auth:api')->get('/ambulance/getAllWorkers', 'API\AmbulanceDashboardController@getAllWorkers');
Route::middleware('auth:api')->delete('/ambulance/fireWorker', 'API\AmbulanceDashboardController@fireWorker');

// Ark routes
Route::middleware('auth:api')->get('/ark/getTopCarModels', 'API\ArkDashboardController@getTopCarModels');
Route::middleware('auth:api')->get('/ark/getSellRequestCounts', 'API\ArkDashboardController@getSellRequestCounts');

// Mechanic routes
Route::middleware('auth:api')->get('/mechanic/getTopWorkers', 'API\Mechanic\MechanicDashboardController@getTopWorkers');
Route::middleware('auth:api')->get('/mechanic/getTopDebtors', 'API\Mechanic\MechanicDashboardController@getTopDebtors');
Route::middleware('auth:api')->post('/mechanic/createReport', 'API\Mechanic\ReportsController@createReport');
Route::middleware('auth:api')->get('/mechanic/getMechanicWorkers', 'API\Mechanic\WorkersController@getWorkersList');
Route::middleware('auth:api')->post('/mechanic/workList/add', 'API\Mechanic\PriceListController@add');
Route::middleware('auth:api')->get('/mechanic/workList/get', 'API\Mechanic\PriceListController@getWorkList');
Route::middleware('auth:api')->put('/mechanic/workList/update', 'API\Mechanic\PriceListController@update');
Route::middleware('auth:api')->delete('/mechanic/workList/delete', 'API\Mechanic\PriceListController@delete');
Route::middleware('auth:api')->get('/mechanic/workList/search', 'API\Mechanic\PriceListController@search');
Route::middleware('auth:api')->get('/mechanic/parts/search', 'API\Mechanic\ReportsController@searchParts');
Route::middleware('auth:api')->post('/mechanic/parts/save', 'API\Mechanic\ReportsController@savePart');

// Broker routes
Route::middleware('auth:api')->get('/broker/getAvailablePropertiesForDashboard', 'API\Broker\BrokerDashboardController@getAvailablePropertiesForDashboard');
Route::middleware('auth:api')->get('/broker/getAvailableProperties', 'API\Broker\BrokerDashboardController@getAvailableProperties');
Route::middleware('auth:api')->get('/broker/getAllPropertiesForDashboard', 'API\Broker\BrokerDashboardController@getAllPropertiesForDashboard');
Route::middleware('auth:api')->get('/broker/getAllProperties', 'API\Broker\BrokerDashboardController@getAllProperties');
Route::middleware('auth:api')->get('/broker/getProperty', 'API\Broker\BrokerController@getProperty');
Route::middleware('auth:api')->put('/broker/changePropertyPrice', 'API\Broker\BrokerController@changePropertyPrice');
Route::middleware('auth:api')->delete('/broker/buyBackProperty', 'API\Broker\BrokerController@buyBackProperty');
Route::middleware('auth:api')->put('/broker/sellProperty', 'API\Broker\BrokerController@sellProperty');
Route::middleware('auth:api')->post('/broker/giveKey', 'API\Broker\BrokerController@giveKey');
Route::middleware('auth:api')->delete('/broker/removeKey', 'API\Broker\BrokerController@removeKey');

//Medic routes
Route::middleware('auth:api')->post('/medic/createReport', 'API\Medic\ReportsController@createReport');
Route::middleware('auth:api')->delete('/medic/deleteRaport', 'API\Medic\ReportsController@deleteRaport');

//polce  routes
Route::middleware('auth:api')->get('/police/getUserFines', 'API\Police\ReportsController@getUserFines');
Route::middleware('auth:api')->get('/police/searchFines', 'API\Police\ReportsController@searchFines');
Route::middleware('auth:api')->post('/police/addFineBill', 'API\Police\ReportsController@addFineBill');
Route::middleware('auth:api')->post('/police/saveReport', 'API\Police\ReportsController@saveReport');
Route::middleware('auth:api')->delete('/police/deleteRaport', 'API\Police\ReportsController@deleteRaport');
Route::middleware('auth:api')->post('/police/declareUserAsWanted', 'API\Police\ReportsController@declareUserAsWanted');
Route::middleware('auth:api')->delete('/police/removeWantedFromperson', 'API\Police\ReportsController@removeWantedFromperson');

// police license manager routes
Route::middleware('auth:api')->post('/police/licenseManager/give', 'API\Police\LicenseController@give');
Route::middleware('auth:api')->delete('/police/licenseManager/remove', 'API\Police\LicenseController@remove');

// Search route
Route::middleware('auth:api')->post('/search/getResults', 'API\SearchController@getSearchResults');
Route::middleware('auth:api')->post('/search/webUser', 'API\SearchController@searchWebUser');

// user profile info
Route::middleware('auth:api')->get('/user/getUserData', 'API\UserController@getUserData');
Route::middleware('auth:api')->get('/user/getCompanyMoney', 'API\UserController@getCompanyMoney');
Route::middleware('auth:api')->get('/user/getJobLabel', 'API\UserController@getJobLabel');

//cars route
Route::middleware('auth:api')->post('/cars/changeModelName', 'API\CarsController@changeModelName');
Route::middleware('auth:api')->post('/cars/addInsurance', 'API\CarsController@addInsurance');
Route::middleware('auth:api')->post('/cars/addCarAsStolen', 'API\CarsController@addCarAsStolen');
Route::middleware('auth:api')->post('/cars/addWantedCar', 'API\CarsController@addWantedCar');
Route::middleware('auth:api')->put('/cars/removeCarAsStolen', 'API\CarsController@removeCarAsStolen');
Route::middleware('auth:api')->put('/cars/removeWantedCar', 'API\CarsController@removeWantedCar');
Route::middleware('auth:api')->put('/cars/confiscateCar', 'API\CarsController@confiscateCar');
Route::middleware('auth:api')->put('/cars/sellCar', 'API\CarsController@sellCar');

// Work application routes
Route::middleware('auth:api')->post('/workApplication/save', 'API\WorkAppController@save');
Route::middleware('auth:api')->get('/workApplication/myApplications', 'API\WorkAppController@getMyWorkApplications');
Route::middleware('auth:api')->get('/workApplication/getWorkApplications', 'API\WorkAppController@getWorkApplications');
Route::middleware('auth:api')->put('/workApplication/accept', 'API\WorkAppController@accept');
Route::middleware('auth:api')->put('/workApplication/reject', 'API\WorkAppController@reject');

//Gabg application routes
Route::middleware('auth:api')->post('/gangApplication/save', 'API\GangApplicationController@save');
Route::middleware('auth:api')->get('/gangApplication/get', 'API\GangApplicationController@getGangApplications');
Route::middleware('auth:api')->get('/gangApplication/getCounts', 'API\GangApplicationController@getGangApplicationsCounts');
Route::middleware('auth:api')->put('/gangApplication/accept', 'API\GangApplicationController@accept');
Route::middleware('auth:api')->put('/gangApplication/reject', 'API\GangApplicationController@reject');

//addon acounts
Route::middleware('auth:api')->get('/sociaty/getAcounts', 'API\SociatyController@acounts');

// billing
Route::middleware('auth:api')->post('/billing/createMechanicBill', 'API\BillsController@createMechanicBill');
Route::middleware('auth:api')->post('/billing/createPoliceBill', 'API\BillsController@createPoliceBill');
Route::middleware('auth:api')->post('/billing/createAmbulanceBill', 'API\BillsController@createPoliceBill');

// Admin routes
Route::middleware('auth:api')->get('/admin/whitelist/getCount', 'API\WhitelistController@getCounts');
Route::middleware('auth:api')->get('/admin/whitelist/getWaitingApplications', 'API\WhitelistController@getWaitingApplications');
Route::middleware('auth:api')->get('/admin/whitelist/getInChatApplications', 'API\WhitelistController@getinChatApplications');
Route::middleware('auth:api')->put('/admin/whitelist/accept', 'API\WhitelistController@acceptWhitelistApplication');
Route::middleware('auth:api')->put('/admin/whitelist/rejected', 'API\WhitelistController@rejectWhitelistApplication');
Route::middleware('auth:api')->put('/admin/whitelist/invietToChat', 'API\WhitelistController@inviteToChat');
Route::middleware('auth:api')->post('/admin/rules/add', 'API\Admin\RulesController@addRule');
Route::middleware('auth:api')->put('/admin/rules/update', 'API\Admin\RulesController@update');
Route::middleware('auth:api')->delete('/admin/rules/delete', 'API\Admin\RulesController@delete');
Route::middleware('auth:api')->get('/admin/rules/searchCategory', 'API\Admin\RulesController@searchCategory');
Route::middleware('auth:api')->post('/admin/info/add', 'API\Admin\InfoController@addInfo');
Route::middleware('auth:api')->put('/admin/info/update', 'API\Admin\InfoController@update');
Route::middleware('auth:api')->delete('/admin/info/delete', 'API\Admin\InfoController@delete');
Route::middleware('auth:api')->get('/admin/info/searchCategory', 'API\Admin\InfoController@searchCategory');
Route::middleware('auth:api')->post('/admin/penalty/addPunishment', 'API\Admin\PenaltyController@addPunishment');
Route::middleware('auth:api')->put('/admin/penalty/addWhitelistBan', 'API\Admin\PenaltyController@addWhitelistBan');
Route::middleware('auth:api')->put('/admin/penalty/removeWhitelistBan', 'API\Admin\PenaltyController@removeWhitelistBan');
Route::middleware('auth:api')->get('/admin/priority/getList', 'API\Admin\PriorityController@getPriorityList');
Route::middleware('auth:api')->post('/admin/priority/add', 'API\Admin\PriorityController@add');
Route::middleware('auth:api')->get('/admin/priority/getCounts', 'API\Admin\PriorityController@getCounts');
Route::middleware('auth:api')->delete('/admin/priority/delete', 'API\Admin\PriorityController@delete');
Route::middleware('auth:api')->get('/admin/donation/getPendingDonations', 'API\Admin\DonationController@getPendingDonations');
Route::middleware('auth:api')->delete('/admin/donation/deleteDonation', 'API\Admin\DonationController@deleteDonation');
Route::middleware('auth:api')->post('/admin/donation/confirmDonation', 'API\Admin\DonationController@confirmDonation');
Route::middleware('auth:api')->get('/admin/donation/getWaitingGift', 'API\Admin\DonationController@getWaitingGiftDonations');
Route::middleware('auth:api')->post('/admin/donation/confirmGift', 'API\Admin\DonationController@confirmGift');
Route::middleware('auth:api')->get('/admin/donation/getConfirmedDonations', 'API\Admin\DonationController@getConfirmedDonations');
Route::middleware('auth:api')->get('/admin/donation/getDonationsSum', 'API\Admin\DonationController@getThisMonthDonationsSum');
Route::middleware('auth:api')->post('/admin/donation/updateGoal', 'API\Admin\DonationController@updateGoal');
Route::middleware('auth:api')->put('/admin/priority/update', 'API\Admin\PriorityController@update');

Route::middleware('auth:api')->get('/logs/getLogs', 'API\Logs\LogsController@getLogs');
